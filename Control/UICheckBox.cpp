#include "stdafx.h"
#include "UICheckBox.h"

namespace DuiLib
{
    LPCTSTR CCheckBoxUI::GetClass() const
    {
        return DUI_CTR_CHECKBOX;
    }

    LPVOID CCheckBoxUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_CHECKBOX) == 0) return static_cast<CCheckBoxUI*>(this);
        return COptionUI::GetInterface(pstrName);
    }

    void CCheckBoxUI::SetCheck(bool bCheck, bool bTriggerEvent)
    {
        Selected(bCheck, bTriggerEvent);
    }

    bool  CCheckBoxUI::GetCheck() const
    {
        return IsSelected();
    }

    void CCheckBoxUI::SetCheckType(CheckBoxType new_type)
    {
        m_type = new_type;
    }

    DuiLib::CheckBoxType CCheckBoxUI::GetCheckType() const
    {
        return m_type;
    }

    void CCheckBoxUI::CheckSelectNodeInfo(bool select_state)
    {
        do 
        {
            if (m_type != CheckBoxType::kTreeNodeCheck)
                break;

            if (nullptr == m_pOwner)
                break;

            m_pOwner->GetTreeView()->CheckSelected(m_pOwner, select_state);

            if (nullptr == m_pOwner->GetParentNode())
            {
                break;
            }

            if (select_state)
            {
                m_pOwner->GetParentNode()->AddSelectedChild(m_pOwner);
            }
            else
            {
                m_pOwner->GetParentNode()->RemoveSelectedChild(m_pOwner);
            }


        } while (0);
    }

    void CCheckBoxUI::SetOnwer(DuiLib::CTreeNodeUI* pOwner)
    {
        m_pOwner = pOwner;
    }

    DuiLib::CTreeNodeUI* CCheckBoxUI::GetOnwer() const
    {
        return m_pOwner;
    }

}

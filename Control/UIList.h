#ifndef __UILIST_H__
#define __UILIST_H__

#pragma once
#include "Layout/UIVerticalLayout.h"
#include "Layout/UIHorizontalLayout.h"

namespace DuiLib
{
    /////////////////////////////////////////////////////////////////////////////////////
    //

    typedef int (CALLBACK* PULVCompareFunc)(UINT_PTR, UINT_PTR, UINT_PTR);

    class CListHeaderUI;
    class CListHeaderUI;
    class CListUI;
#define UILIST_MAX_COLUMNS 64

    typedef struct tagTListInfoUI
    {
        int nColumns = -1;
        RECT rcColumn[UILIST_MAX_COLUMNS] = { 0 };
        UINT uFixedHeight = 0;
        int nFont = -1;
        UINT uTextStyle = 0;
        RECT rcTextPadding = { 0 };
        DWORD dwTextColor = 0;
        DWORD dwBkColor = 0;
        TDrawInfo diBk = { 0 };
        bool bAlternateBk = false;
        DWORD dwSelectedTextColor = 0;
        DWORD dwSelectedBkColor = 0;
        TDrawInfo diSelected = { 0 };
        DWORD dwHotTextColor = 0;
        DWORD dwHotBkColor = 0;
        TDrawInfo diHot = { 0 };
        DWORD dwDisabledTextColor = 0;
        DWORD dwDisabledBkColor = 0;
        TDrawInfo diDisabled;
        int iHLineSize = -1;
        DWORD dwHLineColor = 0;
        int iVLineSize = -1;
        DWORD dwVLineColor = 0;
        bool bShowHtml = false;
        bool bMultiExpandable = false;
    } TListInfoUI;


    /////////////////////////////////////////////////////////////////////////////////////
    //

    class IListCallbackUI
    {
    public:
        virtual LPCTSTR GetItemText(CControlUI* pList, int iItem, int iSubItem) = 0;
    };

    class IListOwnerUI
    {
    public:
        virtual TListInfoUI* GetListInfo() = 0;
        virtual int GetCurSel() const = 0;
        virtual bool SelectItem(int iIndex, bool bTakeFocus = false, bool bTriggerEvent = true) = 0;
        virtual void DoEvent(TEventUI& event) = 0;
        virtual bool ExpandItem(int iIndex, bool bExpand = true) = 0;
        virtual int GetExpandedItem() const = 0;
        virtual bool SelectRange(int iIndex, bool bTakeFocus = false) = 0;
    };

    class IListUI : public IListOwnerUI
    {
    public:
        virtual CListHeaderUI* GetHeader() const = 0;
        virtual CContainerUI* GetList() const = 0;
        virtual IListCallbackUI* GetTextCallback() const = 0;
        virtual void SetTextCallback(IListCallbackUI* pCallback) = 0;
    };

    class IListItemUI
    {
    public:
        virtual int GetIndex() const = 0;
        virtual void SetIndex(int iIndex) = 0;
        virtual int GetDrawIndex() const = 0;
        virtual void SetDrawIndex(int iIndex) = 0;
        virtual IListOwnerUI* GetOwner() = 0;
        virtual void SetOwner(CControlUI* pOwner) = 0;
        virtual bool IsSelected() const = 0;
        virtual bool Select(bool bSelect = true, bool bCallback = true, bool bTriggerEvent = true) = 0;
        virtual bool IsExpanded() const = 0;
        virtual bool Expand(bool bExpand = true) = 0;
        virtual void DrawItemText(HDC hDC, const RECT& rcItem) = 0;
    };


    /////////////////////////////////////////////////////////////////////////////////////
    //
    class CListBodyUI : public CVerticalLayoutUI
    {
    public:
        CListBodyUI(CListUI* pOwner);
        LPCTSTR GetClass() const;
        void SetScrollPos(SIZE szPos);
        void SetPos(RECT rc, bool bNeedInvalidate = true);
        void DoEvent(TEventUI& event);
        bool DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl);
        bool SortItems(PULVCompareFunc pfnCompare, UINT_PTR dwData, int& iCurSel);
        void SetEnabled(bool new_status);
        void GetAllItems(std::vector<CControlUI*>& out);
        void ResetAllItemIndex(const std::vector<CControlUI*>& in);
    protected:
        static int __cdecl ItemComareFunc(void* pvlocale, const void* item1, const void* item2);
        int __cdecl ItemComareFunc(const void* item1, const void* item2);

    protected:
        CListUI* m_pOwner;
        PULVCompareFunc m_pCompareFunc;
        UINT_PTR m_compareData;
    };

    class DUILIB_API CListUI : public CVerticalLayoutUI, public IListUI
    {
    public:
        CListUI();

        LPCTSTR GetClass() const;
        UINT GetControlFlags() const;
        LPVOID GetInterface(LPCTSTR pstrName);

        bool GetScrollSelect();
        void SetScrollSelect(bool bScrollSelect);
        int GetCurSel() const;
        unsigned int GetSelectedCount()const;
        virtual bool SelectItem(int iIndex, bool bTakeFocus = false, bool bTriggerEvent = true);
        bool SelectRange(int iIndex, bool bTakeFocus);

        CControlUI* GetItemAt(int iIndex) const;
        int GetItemIndex(CControlUI* pControl) const;
        bool SetItemIndex(CControlUI* pControl, int iIndex);
        bool SetMultiItemIndex(CControlUI* pStartControl, int iCount, int iNewStartIndex);
        int GetCount() const;
        bool Add(CControlUI* pControl);
        bool AddAt(CControlUI* pControl, int iIndex);
        bool AddRanges(CControlUI* pStartControl[], int start_index, int count);
        bool Remove(CControlUI* pControl, bool bDoNotDestroy = false);
        bool RemoveAt(int iIndex, bool bDoNotDestroy = false);
        bool RemoveRanges(int start_index, int count, bool /*bDoNotDestroy = true*/);
        void RemoveAll();

        void EnsureVisible(int iIndex);
        void Scroll(int dx, int dy);

        int GetChildPadding() const;
        void SetChildPadding(int iPadding);

        CListHeaderUI* GetHeader() const;
        CContainerUI* GetList() const;
        TListInfoUI* GetListInfo();

        UINT GetItemFixedHeight();
        void SetItemFixedHeight(UINT nHeight);
        int GetItemFont(int index);
        void SetItemFont(int index);
        UINT GetItemTextStyle();
        void SetItemTextStyle(UINT uStyle);
        RECT GetItemTextPadding() const;
        void SetItemTextPadding(RECT rc);
        DWORD GetItemTextColor() const;
        void SetItemTextColor(DWORD dwTextColor);
        DWORD GetItemBkColor() const;
        void SetItemBkColor(DWORD dwBkColor);
        LPCTSTR GetItemBkImage() const;
        void SetItemBkImage(LPCTSTR pStrImage);
        bool IsAlternateBk() const;
        void SetAlternateBk(bool bAlternateBk);
        DWORD GetSelectedItemTextColor() const;
        void SetSelectedItemTextColor(DWORD dwTextColor);
        DWORD GetSelectedItemBkColor() const;
        void SetSelectedItemBkColor(DWORD dwBkColor);
        LPCTSTR GetSelectedItemImage() const;
        void SetSelectedItemImage(LPCTSTR pStrImage);
        DWORD GetHotItemTextColor() const;
        void SetHotItemTextColor(DWORD dwTextColor);
        DWORD GetHotItemBkColor() const;
        void SetHotItemBkColor(DWORD dwBkColor);
        LPCTSTR GetHotItemImage() const;
        void SetHotItemImage(LPCTSTR pStrImage);
        DWORD GetDisabledItemTextColor() const;
        void SetDisabledItemTextColor(DWORD dwTextColor);
        DWORD GetDisabledItemBkColor() const;
        void SetDisabledItemBkColor(DWORD dwBkColor);
        LPCTSTR GetDisabledItemImage() const;
        void SetDisabledItemImage(LPCTSTR pStrImage);
        int GetItemHLineSize() const;
        void SetItemHLineSize(int iSize);
        DWORD GetItemHLineColor() const;
        void SetItemHLineColor(DWORD dwLineColor);
        int GetItemVLineSize() const;
        void SetItemVLineSize(int iSize);
        DWORD GetItemVLineColor() const;
        void SetItemVLineColor(DWORD dwLineColor);
        bool IsItemShowHtml();
        void SetItemShowHtml(bool bShowHtml = true);

        void SetMultiExpanding(bool bMultiExpandable);
        int GetExpandedItem() const;
        bool ExpandItem(int iIndex, bool bExpand = true);

        void SetPos(RECT rc, bool bNeedInvalidate = true);
        void Move(SIZE szOffset, bool bNeedInvalidate = true);
        void DoEvent(TEventUI& event);
        void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

        IListCallbackUI* GetTextCallback() const;
        void SetTextCallback(IListCallbackUI* pCallback);

        SIZE GetScrollPos() const;
        SIZE GetScrollRange() const;
        void SetScrollPos(SIZE szPos);
        void LineUp();
        void LineDown();
        void PageUp();
        void PageDown();
        void HomeUp();
        void EndDown();
        void LineLeft();
        void LineRight();
        void PageLeft();
        void PageRight();
        void HomeLeft();
        void EndRight();
        void EnableScrollBar(bool bEnableVertical = true, bool bEnableHorizontal = false);
        virtual CScrollBarUI* GetVerticalScrollBar() const;
        virtual CScrollBarUI* GetHorizontalScrollBar() const;
        virtual bool SortItems(PULVCompareFunc pfnCompare, UINT_PTR dwData);

        // 是否是List的子控件
        bool IsSubControl(CControlUI* pControl);
        void SetLockSelectedItem(bool lock);
        bool GetIsLockSelectedItem()const;
        CControlUI* GetMouseSelectRect()const;
        void SetName(LPCTSTR pstrName) override;
        int GetColumnCount()const;

        void GetWholeItem(std::vector<CControlUI*>& out);
        void ResetWholeItemIndex(const std::vector<CControlUI*>& in);
    protected:


    private:
        bool SelectItemWhenCtrlKeyDown(int dest_control_index, bool bTakeFocus = false, bool bTriggerEvent = true);
    protected:
        bool m_LockSelectedItem = false;
        bool m_bScrollSelect = false;
        int m_iCurSel = -1;
        int m_iExpandedItem = -1;
        IListCallbackUI* m_pCallback = nullptr;
        CListBodyUI* m_pList = nullptr;
        CListHeaderUI* m_pHeader = nullptr;
        TListInfoUI m_ListInfo = { 0 };

        POINT m_ptDown = { 0 };
        POINT m_ptMove = { 0 };

        CControlUI* m_pSelectRect = nullptr;

        CDuiPtrArray m_SelectedItems = { 0 };
    };

    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListHeaderUI : public CHorizontalLayoutUI
    {
    public:
        CListHeaderUI();

        LPCTSTR GetClass() const;
        LPVOID GetInterface(LPCTSTR pstrName);

        SIZE EstimateSize(SIZE szAvailable);
    };


    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListHeaderItemUI : public CControlUI
    {
    public:
        enum class CompareType
        {
            kNoCompare = -1,
            kDate,
            kTime,
            kText,
            kNumerical,
            kIpv4,
        };

        CListHeaderItemUI();

        LPCTSTR GetClass() const;
        LPVOID GetInterface(LPCTSTR pstrName);
        UINT GetControlFlags() const;

        void SetEnabled(bool bEnable = true);

        bool IsDragable() const;
        void SetDragable(bool bDragable);
        int GetIndex() const;
        void SetIndex(int new_index);
        DWORD GetSepWidth() const;
        void SetSepWidth(int iWidth);
        DWORD GetTextStyle() const;
        void SetTextStyle(UINT uStyle);
        DWORD GetTextColor() const;
        void SetTextColor(DWORD dwTextColor);
        DWORD GetSepColor() const;
        void SetSepColor(DWORD dwSepColor);
        void SetTextPadding(RECT rc);
        RECT GetTextPadding() const;
        void SetFont(int index);
        bool IsShowHtml();
        void SetShowHtml(bool bShowHtml = true);
        LPCTSTR GetNormalImage() const;
        void SetNormalImage(LPCTSTR pStrImage);
        LPCTSTR GetHotImage() const;
        void SetHotImage(LPCTSTR pStrImage);
        LPCTSTR GetPushedImage() const;
        void SetPushedImage(LPCTSTR pStrImage);
        LPCTSTR GetFocusedImage() const;
        void SetFocusedImage(LPCTSTR pStrImage);
        LPCTSTR GetSepImage() const;
        void SetSepImage(LPCTSTR pStrImage);

        void DoEvent(TEventUI& event);
        SIZE EstimateSize(SIZE szAvailable);
        void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);
        RECT GetThumbRect() const;

        void PaintText(HDC hDC);
        void PaintStatusImage(HDC hDC);

        void SetCompareType(CompareType new_compare_type);
        CompareType GetCompareType()const;

    protected:
        POINT ptLastMouse;
        bool m_bDragable;
        UINT m_uButtonState;
        int m_iSepWidth;
        DWORD m_dwTextColor;
        DWORD m_dwSepColor;
        int m_iFont;
        UINT m_uTextStyle;
        bool m_bShowHtml;
        int m_Index = -1;
        bool m_Rearrange = false;
        RECT m_rcTextPadding;
        CompareType m_CompareType = CompareType::kText;
        TDrawInfo m_diNormal;
        TDrawInfo m_diHot;
        TDrawInfo m_diPushed;
        TDrawInfo m_diFocused;
        TDrawInfo m_diSep;
    };


    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListElementUI : public CControlUI, public IListItemUI
    {
    public:
        CListElementUI();

        LPCTSTR GetClass() const;
        UINT GetControlFlags() const;
        LPVOID GetInterface(LPCTSTR pstrName);

        void SetEnabled(bool bEnable = true);

        int GetIndex() const;
        void SetIndex(int iIndex);
        int GetDrawIndex() const;
        void SetDrawIndex(int iIndex);

        IListOwnerUI* GetOwner();
        void SetOwner(CControlUI* pOwner);
        void SetVisible(bool bVisible = true);

        bool IsSelected() const;
        bool Select(bool bSelect = true, bool bCallback = true, bool bTriggerEvent = true);
        bool IsExpanded() const;
        bool Expand(bool bExpand = true);

        void Invalidate(); // 直接CControl::Invalidate会导致滚动条刷新，重写减少刷新区域
        bool Activate();

        void DoEvent(TEventUI& event);
        void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

        void DrawItemBk(HDC hDC, const RECT& rcItem);

        bool Hot(bool bHot);
        bool IsHot();

    protected:
        int m_iIndex = -1;
        int m_iDrawIndex;
        bool m_bSelected;
        bool m_Hot = false;
        UINT m_uButtonState;
        IListOwnerUI* m_pOwner;
    };


    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListLabelElementUI : public CListElementUI
    {
    public:
        CListLabelElementUI();

        LPCTSTR GetClass() const;
        LPVOID GetInterface(LPCTSTR pstrName);

        void SetOwner(CControlUI* pOwner);

        void SetFixedWidth(int cx);
        void SetFixedHeight(int cy);
        void SetText(LPCTSTR pstrText);

        void DoEvent(TEventUI& event);
        SIZE EstimateSize(SIZE szAvailable);
        bool DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl);

        void DrawItemText(HDC hDC, const RECT& rcItem);

    protected:
        SIZE    m_cxyFixedLast;
        bool    m_bNeedEstimateSize;

        SIZE    m_szAvailableLast;
        UINT    m_uFixedHeightLast;
        int     m_nFontLast;
        UINT    m_uTextStyleLast;
        RECT    m_rcTextPaddingLast;
    };


    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListTextElementUI : public CListLabelElementUI
    {
    public:
        CListTextElementUI();
        ~CListTextElementUI();

        LPCTSTR GetClass() const;
        LPVOID GetInterface(LPCTSTR pstrName);
        UINT GetControlFlags() const;

        LPCTSTR GetText(int iIndex);
        void SetText(int iIndex, LPCTSTR pstrText);

        void SetOwner(CControlUI* pOwner);
        CDuiString* GetLinkContent(int iIndex);

        void DoEvent(TEventUI& event);
        SIZE EstimateSize(SIZE szAvailable);

        void DrawItemText(HDC hDC, const RECT& rcItem);
        void ReDrawItemText();
        void InitItemText();
    protected:
        enum
        {
            MAX_LINK = 8
        };
        int m_nLinks;
        RECT m_rcLinks[MAX_LINK];
        CDuiString m_sLinks[MAX_LINK];
        int m_nHoverLink;
        IListUI* m_pOwner;
        CDuiPtrArray m_aTexts;
        HDC m_hDC;
        RECT m_RercItem;
        CDuiString m_sTextLast;
        std::string m_CompareValue;
    };

    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListContainerElementUI : public CContainerUI, public IListItemUI
    {
    public:
        CListContainerElementUI();

        LPCTSTR GetClass() const;
        UINT GetControlFlags() const;
        LPVOID GetInterface(LPCTSTR pstrName);

        int GetIndex() const;
        void SetIndex(int iIndex);
        int GetDrawIndex() const;
        void SetDrawIndex(int iIndex);

        IListOwnerUI* GetOwner();
        void SetOwner(CControlUI* pOwner);
        void SetVisible(bool bVisible = true);
        void SetEnabled(bool bEnable = true);

        bool IsSelected() const;
        bool Select(bool bSelect = true, bool bCallback = true, bool bTriggerEvent = true);
        bool IsExpandable() const;
        void SetExpandable(bool bExpandable);
        bool IsExpanded() const;
        bool Expand(bool bExpand = true);

        void Invalidate(); // 直接CControl::Invalidate会导致滚动条刷新，重写减少刷新区域
        bool Activate();

        void DoEvent(TEventUI& event);
        void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);
        bool DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl);

        void DrawItemText(HDC hDC, const RECT& rcItem);
        void DrawItemBk(HDC hDC, const RECT& rcItem);

        SIZE EstimateSize(SIZE szAvailable);

    protected:
        int m_iIndex;
        int m_iDrawIndex;
        bool m_bSelected;
        bool m_bExpandable;
        bool m_bExpand;
        UINT m_uButtonState;
        IListOwnerUI* m_pOwner;
    };

    /////////////////////////////////////////////////////////////////////////////////////
    //

    class DUILIB_API CListHBoxElementUI : public CListContainerElementUI
    {
    public:
        CListHBoxElementUI();

        LPCTSTR GetClass() const;
        LPVOID GetInterface(LPCTSTR pstrName);

        void SetPos(RECT rc, bool bNeedInvalidate = true);
        bool DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl);
    };
} // namespace DuiLib

#endif // __UILIST_H__

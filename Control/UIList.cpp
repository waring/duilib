#include "StdAfx.h"
namespace DuiLib
{
    /////////////////////////////////////////////////////////////////////////////////////
    //
    //
    CListUI::CListUI() : m_pCallback(NULL), m_bScrollSelect(false), m_iCurSel(-1), m_iExpandedItem(-1), m_pSelectRect(NULL)
    {
        try
        {
            m_pList = new CListBodyUI(this);
            m_pHeader = new CListHeaderUI;
        }
        catch (...) // 此处曾有不明原因异常抛出
        {
            bool catch_exception = true;
        }


        Add(m_pHeader);
        CVerticalLayoutUI::Add(m_pList);

        m_ListInfo.nColumns = 0;
        m_ListInfo.uFixedHeight = 0;
        m_ListInfo.nFont = -1;
        m_ListInfo.uTextStyle = DT_VCENTER | DT_SINGLELINE; // m_uTextStyle(DT_VCENTER | DT_END_ELLIPSIS)
        m_ListInfo.dwTextColor = 0xFF000000;
        m_ListInfo.dwBkColor = 0;
        m_ListInfo.bAlternateBk = false;
        m_ListInfo.dwSelectedTextColor = 0xFF000000;
        m_ListInfo.dwSelectedBkColor = 0xFFC1E3FF;
        m_ListInfo.dwHotTextColor = 0xFF000000;
        m_ListInfo.dwHotBkColor = 0xFFE9F5FF;
        m_ListInfo.dwDisabledTextColor = 0xFFCCCCCC;
        m_ListInfo.dwDisabledBkColor = 0xFFFFFFFF;
        m_ListInfo.iHLineSize = 0;
        m_ListInfo.dwHLineColor = 0xFF3C3C3C;
        m_ListInfo.iVLineSize = 0;
        m_ListInfo.dwVLineColor = 0xFF3C3C3C;
        m_ListInfo.bShowHtml = false;
        m_ListInfo.bMultiExpandable = false;
        ::ZeroMemory(&m_ListInfo.rcTextPadding, sizeof(m_ListInfo.rcTextPadding));
        ::ZeroMemory(&m_ListInfo.rcColumn, sizeof(m_ListInfo.rcColumn));
    }

    LPCTSTR CListUI::GetClass() const
    {
        return DUI_CTR_LIST;
    }

    UINT CListUI::GetControlFlags() const
    {
        return UIFLAG_TABSTOP;
    }

    LPVOID CListUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_LIST) == 0) return static_cast<CListUI*>(this);
        if (_tcscmp(pstrName, DUI_CTR_ILIST) == 0) return static_cast<IListUI*>(this);
        if (_tcscmp(pstrName, DUI_CTR_ILISTOWNER) == 0) return static_cast<IListOwnerUI*>(this);
        return CVerticalLayoutUI::GetInterface(pstrName);
    }

    CControlUI* CListUI::GetItemAt(int iIndex) const
    {
        return m_pList->GetItemAt(iIndex);
    }

    int CListUI::GetItemIndex(CControlUI* pControl) const
    {
        if (pControl->GetInterface(DUI_CTR_LISTHEADER) != NULL) 
            return CVerticalLayoutUI::GetItemIndex(pControl);

        // We also need to recognize header sub-items
        if (_tcsstr(pControl->GetClass(), DUI_CTR_LISTHEADERITEM) != NULL) 
            return m_pHeader->GetItemIndex(pControl);

        return m_pList->GetItemIndex(pControl);
    }

    bool CListUI::SetItemIndex(CControlUI* pControl, int iIndex)
    {
        if (pControl->GetInterface(DUI_CTR_LISTHEADER) != NULL) 
            return CVerticalLayoutUI::SetItemIndex(pControl, iIndex);
        // We also need to recognize header sub-items
        if (_tcsstr(pControl->GetClass(), DUI_CTR_LISTHEADERITEM) != NULL) 
            return m_pHeader->SetItemIndex(pControl, iIndex);

        int iOrginIndex = m_pList->GetItemIndex(pControl);
        if (iOrginIndex == -1) 
            return false;
        if (iOrginIndex == iIndex) 
            return true;

        IListItemUI* pSelectedListItem = NULL;
        if (m_iCurSel >= 0) pSelectedListItem =
            static_cast<IListItemUI*>(GetItemAt(m_iCurSel)->GetInterface(DUI_CTR_ILISTITEM));
        if (!m_pList->SetItemIndex(pControl, iIndex)) 
            return false;
        int iMinIndex = min(iOrginIndex, iIndex);
        int iMaxIndex = max(iOrginIndex, iIndex);
        for (int i = iMinIndex; i < iMaxIndex + 1; ++i)
        {
            CControlUI* p = m_pList->GetItemAt(i);
            IListItemUI* pListItem = static_cast<IListItemUI*>(p->GetInterface(DUI_CTR_ILISTITEM));
            if (pListItem != NULL)
            {
                pListItem->SetIndex(i);
            }
        }
        if (m_iCurSel >= 0 && pSelectedListItem != NULL) 
            m_iCurSel = pSelectedListItem->GetIndex();
        return true;
    }

    bool CListUI::SetMultiItemIndex(CControlUI* pStartControl, int iCount, int iNewStartIndex)
    {
        if (pStartControl == NULL || iCount < 0 || iNewStartIndex < 0) 
            return false;
        if (pStartControl->GetInterface(DUI_CTR_LISTHEADER) != NULL) 
            return CVerticalLayoutUI::SetMultiItemIndex(pStartControl, iCount, iNewStartIndex);
        // We also need to recognize header sub-items
        if (_tcsstr(pStartControl->GetClass(), DUI_CTR_LISTHEADERITEM) != NULL) 
            return m_pHeader->SetMultiItemIndex(pStartControl, iCount, iNewStartIndex);

        int iStartIndex = GetItemIndex(pStartControl);
        if (iStartIndex == iNewStartIndex) 
            return true;
        if (iStartIndex + iCount > GetCount()) 
            return false;
        if (iNewStartIndex + iCount > GetCount()) 
            return false;

        IListItemUI* pSelectedListItem = NULL;
        if (m_iCurSel >= 0) pSelectedListItem =
            static_cast<IListItemUI*>(GetItemAt(m_iCurSel)->GetInterface(DUI_CTR_ILISTITEM));
        if (!m_pList->SetMultiItemIndex(pStartControl, iCount, iNewStartIndex)) 
            return false;
        int iMinIndex = min(iStartIndex, iNewStartIndex);
        int iMaxIndex = max(iStartIndex + iCount, iNewStartIndex + iCount);
        for (int i = iMinIndex; i < iMaxIndex + 1; ++i)
        {
            CControlUI* p = m_pList->GetItemAt(i);
            IListItemUI* pListItem = static_cast<IListItemUI*>(p->GetInterface(DUI_CTR_ILISTITEM));
            if (pListItem != NULL)
            {
                pListItem->SetIndex(i);
            }
        }
        if (m_iCurSel >= 0 && pSelectedListItem != NULL) 
            m_iCurSel = pSelectedListItem->GetIndex();
        return true;
    }

    int CListUI::GetCount() const
    {
        return m_pList->GetCount();
    }

    bool CListUI::Add(CControlUI* pControl)
    {
        // Override the Add() method so we can add items specifically to
        // the intended widgets. Headers are assumed to be
        // answer the correct interface so we can add multiple list headers.
        if (pControl->GetInterface(DUI_CTR_LISTHEADER) != NULL)
        {
            if (m_pHeader != pControl && m_pHeader->GetCount() == 0)
            {
                CVerticalLayoutUI::Remove(m_pHeader);
                m_pHeader = static_cast<CListHeaderUI*>(pControl);
            }
            m_ListInfo.nColumns = MIN(m_pHeader->GetCount(), UILIST_MAX_COLUMNS);
            return CVerticalLayoutUI::AddAt(pControl, 0);
        }
        // We also need to recognize header sub-items
        if (_tcsstr(pControl->GetClass(), DUI_CTR_LISTHEADERITEM) != NULL)
        {
            bool ret = m_pHeader->Add(pControl);
            CListHeaderItemUI* pCListHeaderItemUI = ret ?
                dynamic_cast<CListHeaderItemUI*>(pControl)
                : nullptr;
            pCListHeaderItemUI ?
                pCListHeaderItemUI->SetIndex(m_pHeader->GetCount() - 1)
                : 0;
            m_ListInfo.nColumns = MIN(m_pHeader->GetCount(), UILIST_MAX_COLUMNS);
            return ret;
        }
        // The list items should know about us
        IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
        if (pListItem != NULL)
        {
            pListItem->SetOwner(this);
            pListItem->SetIndex(GetCount());
        }
        return m_pList->Add(pControl);
    }

    bool CListUI::AddAt(CControlUI* pControl, int iIndex)
    {
        // Override the AddAt() method so we can add items specifically to
        // the intended widgets. Headers and are assumed to be
        // answer the correct interface so we can add multiple list headers.
        if (pControl->GetInterface(DUI_CTR_LISTHEADER) != NULL)
        {
            if (m_pHeader != pControl && m_pHeader->GetCount() == 0)
            {
                CVerticalLayoutUI::Remove(m_pHeader);
                m_pHeader = static_cast<CListHeaderUI*>(pControl);
            }
            m_ListInfo.nColumns = MIN(m_pHeader->GetCount(), UILIST_MAX_COLUMNS);
            return CVerticalLayoutUI::AddAt(pControl, 0);
        }
        // We also need to recognize header sub-items
        if (_tcsstr(pControl->GetClass(), DUI_CTR_LISTHEADERITEM) != NULL)
        {
            bool ret = m_pHeader->AddAt(pControl, iIndex);
            m_ListInfo.nColumns = MIN(m_pHeader->GetCount(), UILIST_MAX_COLUMNS);
            return ret;
        }
        if (!m_pList->AddAt(pControl, iIndex)) return false;

        // The list items should know about us
        IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
        if (pListItem != NULL)
        {
            pListItem->SetOwner(this);
            pListItem->SetIndex(iIndex);
        }

        for (int i = iIndex + 1; i < m_pList->GetCount(); ++i)
        {
            CControlUI* p = m_pList->GetItemAt(i);
            pListItem = static_cast<IListItemUI*>(p->GetInterface(DUI_CTR_ILISTITEM));
            if (pListItem != NULL)
            {
                pListItem->SetIndex(i);
            }
        }
        if (m_iCurSel >= iIndex) m_iCurSel += 1;
        return true;
    }

    bool CListUI::AddRanges(CControlUI* pStartControl[], int start_index, int count)
    {
        bool result = false;
        do
        {
            if (!m_pList->AddRanges(pStartControl, start_index, count))
                break;

            IListItemUI* pListItem = nullptr;
            CControlUI* pBaseObject = nullptr;
            int count = m_pList->GetCount();
            for (int i = start_index; i < count; ++i)
            {
                pBaseObject = m_pList->GetItemAt(i);
                pListItem = static_cast<IListItemUI*>(pBaseObject->GetInterface(DUI_CTR_ILISTITEM));
                if (pListItem != NULL)
                {
                    pListItem->SetOwner(this);
                    pListItem->SetIndex(i);
                }
            }

            result = true;
            int new_selecet = GetItemIndex(*pStartControl);
            if (new_selecet > -1)
                result = SelectItem(new_selecet, false, false);
        } while (0);

        return result;
    }

    bool CListUI::Remove(CControlUI* pControl, bool bDoNotDestroy)
    {
        if (pControl->GetInterface(DUI_CTR_LISTHEADER) != NULL)
            return CVerticalLayoutUI::Remove(pControl, bDoNotDestroy);

        // We also need to recognize header sub-items
        if (_tcsstr(pControl->GetClass(), DUI_CTR_LISTHEADERITEM) != NULL)
            return m_pHeader->Remove(pControl, bDoNotDestroy);

        int iIndex = m_pList->GetItemIndex(pControl);
        if (iIndex == -1)
            return false;

        if (!m_pList->RemoveAt(iIndex, bDoNotDestroy))
            return false;

        for (int i = iIndex; i < m_pList->GetCount(); ++i)
        {
            CControlUI* p = m_pList->GetItemAt(i);
            IListItemUI* pListItem = static_cast<IListItemUI*>(p->GetInterface(DUI_CTR_ILISTITEM));
            if (pListItem != NULL)
            {
                pListItem->SetIndex(i);
            }
        }

        if (iIndex == m_iCurSel && m_iCurSel >= 0)
        {
            int iSel = m_iCurSel;
            m_iCurSel = -1;
            SelectItem(FindSelectable(iSel, false));
        }
        else
            if (iIndex < m_iCurSel) m_iCurSel -= 1;
        return true;
    }

    bool CListUI::RemoveAt(int iIndex, bool bDoNotDestroy)
    {
        if (!m_pList->RemoveAt(iIndex, bDoNotDestroy)) return false;

        for (int i = iIndex; i < m_pList->GetCount(); ++i)
        {
            CControlUI* p = m_pList->GetItemAt(i);
            IListItemUI* pListItem = static_cast<IListItemUI*>(p->GetInterface(DUI_CTR_ILISTITEM));
            if (pListItem != NULL) pListItem->SetIndex(i);
        }

        if (iIndex == m_iCurSel && m_iCurSel >= 0)
        {
            int iSel = m_iCurSel;
            m_iCurSel = -1;
            SelectItem(FindSelectable(iSel, false));
        }
        else if (iIndex < m_iCurSel) m_iCurSel -= 1;
        return true;
    }


    bool CListUI::RemoveRanges(int start_index, int count, bool)
    {
        do
        {
            CControlUI* pContrl = nullptr;
            IListItemUI* pListItem = nullptr;
            for (int i = start_index; i < start_index + count; ++i)
            {
                pContrl = m_pList->GetItemAt(i);
                pListItem = static_cast<IListItemUI*>(pContrl->GetInterface(DUI_CTR_ILISTITEM));
                if (pListItem == NULL)
                    goto return_failed;
            }

            if (!m_pList->RemoveRanges(start_index, count, 1))
                goto return_failed;

            for (int i = 0; i < m_pList->GetCount(); ++i)
            {
                pContrl = m_pList->GetItemAt(i);
                pListItem = static_cast<IListItemUI*>(pContrl->GetInterface(DUI_CTR_ILISTITEM));
                if (pListItem != NULL)
                {
                    pListItem->SetIndex(i);
                }
            }

        } while (0);

        return true;
return_failed:
        return false;
    }


    void CListUI::RemoveAll()
    {
        m_iCurSel = -1;
        m_iExpandedItem = -1;
        m_pList->RemoveAll();
    }

    void CListUI::SetPos(RECT rc, bool bNeedInvalidate)
    {
        if (m_pHeader != NULL)
        { // 设置header各子元素x坐标,因为有些listitem的setpos需要用到(临时修复)
            int iLeft = rc.left + m_rcInset.left;
            int iRight = rc.right - m_rcInset.right;

            m_ListInfo.nColumns = MIN(m_pHeader->GetCount(), UILIST_MAX_COLUMNS);

            if (!m_pHeader->IsVisible())
            {
                for (int it = m_pHeader->GetCount() - 1; it >= 0; it--)
                {
                    static_cast<CControlUI*>(m_pHeader->GetItemAt(it))->SetInternVisible(true);
                }
            }
            m_pHeader->SetPos(CDuiRect(iLeft, 0, iRight, 0), false);
            int iOffset = m_pList->GetScrollPos().cx;
            for (int i = 0; i < m_ListInfo.nColumns; i++)
            {
                CControlUI* pControl = static_cast<CControlUI*>(m_pHeader->GetItemAt(i));
                if (!pControl->IsVisible()) continue;
                if (pControl->IsFloat()) continue;

                RECT rcPos = pControl->GetPos();
                if (iOffset > 0)
                {
                    rcPos.left -= iOffset;
                    rcPos.right -= iOffset;
                    pControl->SetPos(rcPos, false);
                }
                m_ListInfo.rcColumn[i] = pControl->GetPos();
            }
            if (!m_pHeader->IsVisible())
            {
                for (int it = m_pHeader->GetCount() - 1; it >= 0; it--)
                {
                    static_cast<CControlUI*>(m_pHeader->GetItemAt(it))->SetInternVisible(false);
                }
                m_pHeader->SetInternVisible(false);
            }
        }

        CVerticalLayoutUI::SetPos(rc, bNeedInvalidate);

        if (m_pHeader == NULL) return;

        rc = m_rcItem;
        rc.left += m_rcInset.left;
        rc.top += m_rcInset.top;
        rc.right -= m_rcInset.right;
        rc.bottom -= m_rcInset.bottom;

        if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible())
        {
            rc.top -= m_pVerticalScrollBar->GetScrollPos();
            rc.bottom -= m_pVerticalScrollBar->GetScrollPos();
            rc.bottom += m_pVerticalScrollBar->GetScrollRange();
            rc.right -= m_pVerticalScrollBar->GetFixedWidth();
        }
        if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible())
        {
            rc.left -= m_pHorizontalScrollBar->GetScrollPos();
            rc.right -= m_pHorizontalScrollBar->GetScrollPos();
            rc.right += m_pHorizontalScrollBar->GetScrollRange();
            rc.bottom -= m_pHorizontalScrollBar->GetFixedHeight();
        }

        m_ListInfo.nColumns = MIN(m_pHeader->GetCount(), UILIST_MAX_COLUMNS);

        if (!m_pHeader->IsVisible())
        {
            for (int it = m_pHeader->GetCount() - 1; it >= 0; it--)
            {
                static_cast<CControlUI*>(m_pHeader->GetItemAt(it))->SetInternVisible(true);
            }
            m_pHeader->SetPos(CDuiRect(rc.left, 0, rc.right, 0), false);
        }
        int iOffset = m_pList->GetScrollPos().cx;
        for (int i = 0; i < m_ListInfo.nColumns; i++)
        {
            CControlUI* pControl = static_cast<CControlUI*>(m_pHeader->GetItemAt(i));
            if (!pControl->IsVisible()) continue;
            if (pControl->IsFloat()) continue;

            RECT rcPos = pControl->GetPos();
            if (iOffset > 0)
            {
                rcPos.left -= iOffset;
                rcPos.right -= iOffset;
                pControl->SetPos(rcPos, false);
            }
            m_ListInfo.rcColumn[i] = pControl->GetPos();
        }
        if (!m_pHeader->IsVisible())
        {
            for (int it = m_pHeader->GetCount() - 1; it >= 0; it--)
            {
                static_cast<CControlUI*>(m_pHeader->GetItemAt(it))->SetInternVisible(false);
            }
            m_pHeader->SetInternVisible(false);
        }
    }

    void CListUI::Move(SIZE szOffset, bool bNeedInvalidate)
    {
        CVerticalLayoutUI::Move(szOffset, bNeedInvalidate);
        if (!m_pHeader->IsVisible()) m_pHeader->Move(szOffset, false);
    }

    void CListUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pParent != NULL) m_pParent->DoEvent(event);
            else CVerticalLayoutUI::DoEvent(event);
            return;
        }

        if (event.Type == UIEVENT_SETFOCUS)
        {
            m_bFocused = true;
            return;
        }
        if (event.Type == UIEVENT_KILLFOCUS)
        {
            m_bFocused = false;
            return;
        }

        if (event.Type == UIEVENT_KEYDOWN)
        {
            if (IsKeyboardEnabled() && IsEnabled())
            {
                switch (event.chKey)
                {
                case VK_UP:
                    SelectItem(FindSelectable(m_iCurSel - 1, false), true);
                    break;
                case VK_DOWN:
                    SelectItem(FindSelectable(m_iCurSel + 1, true), true);
                    break;
                case VK_PRIOR:
                    PageUp();
                    break;
                case VK_NEXT:
                    PageDown();
                    break;
                case VK_HOME:
                    SelectItem(FindSelectable(0, false), true);
                    break;
                case VK_END:
                    SelectItem(FindSelectable(GetCount() - 1, true), true);
                    break;
                case VK_RETURN:
                    if (m_iCurSel != -1) GetItemAt(m_iCurSel)->Activate();
                    break;
                case VK_DELETE:
                    if (m_pManager != nullptr)
                    {
                        m_pManager->SendNotify(this, L"itemdelete", 0, 0);
                    }
                    break;
                }

                BOOL bCtrl = (GetKeyState(VK_CONTROL) & 0x8000);
                if (bCtrl && (event.wParam == 'a' || event.wParam == 'A'))
                {
                    int iOldSel = m_iCurSel;
                    for (int i = 0; i < GetCount(); i++)
                    {
                        auto pItem = GetItemAt(i);
                        IListItemUI* pListItem = static_cast<IListItemUI*>(pItem->GetInterface(DUI_CTR_ILISTITEM));
                        if (pListItem != nullptr)
                        {
                            if (!pListItem->IsSelected())
                            {
                                pListItem->Select(true, false);
                            }
                        }
                    }
                    m_iCurSel = m_iCurSel == -1 ? GetCount() - 1 : m_iCurSel;
                    if (m_pManager != nullptr)
                    {
                        m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMSELECT, m_iCurSel, iOldSel);
                    }
                }
                return;
            }
        }

        if (event.Type == UIEVENT_SCROLLWHEEL)
        {
            if (IsEnabled())
            {
                switch (LOWORD(event.wParam))
                {
                case SB_LINEUP:
                    if (m_bScrollSelect)
                    {
                        SelectItem(FindSelectable(m_iCurSel - 1, false), true);
                    }
                    else
                    {
                        LineUp();
                    }
                    return;
                case SB_LINEDOWN:
                    if (m_bScrollSelect)
                    {
                        SelectItem(FindSelectable(m_iCurSel + 1, true), true);
                    }
                    else
                    {
                        LineDown();
                    }
                    return;
                }
            }
        }
        if (event.Type == UIEVENT_BUTTONDOWN)
        {
            if (IsEnabled())
            {
                m_ptDown = event.ptMouse;
                m_uButtonState |= UISTATE_PUSHED;

                DuiLib::CDuiString pSenderClass = event.pSender->GetClass();
                if (pSenderClass == _T("ListBody"))
                {
                    //debughelper::DebugPrint(0, "event.Type == UIEVENT_BUTTONDOWN. on ListBody\n");
                    this->SelectItem(-1);

                    //auto count = GetCount();
                    //CListTextElementUI* p12 = nullptr;
                    //for (int loop=0;loop< count;loop++)
                    //{
                    //    p12 = (CListTextElementUI*)GetItemAt(loop);
                    //    debughelper::DebugPrint(0, "name = %s, IsSelect = %d, IsHot = %d \n",
                    //                            p12->GetName().GetData(),
                    //                            p12->IsSelected(),
                    //                            p12->IsHot());
                    //}
                }
            }
            return;
        }

        if (event.Type == UIEVENT_RBUTTONDOWN)
        {
            if (IsEnabled() == true)
            {
                DuiLib::CDuiString pSenderClass = event.pSender->GetClass();
                pSenderClass == _T("ListBody") ? this->SelectItem(-1) : 0;
            }
        }

        if (event.Type == UIEVENT_BUTTONUP)
        {
            if (IsEnabled())
            {
                if (!(m_uButtonState & UISTATE_SELECTED))
                {
                    SelectItem(-1);
                    m_pManager->SendNotify(this, DUI_MSGTYPE_CLICK);
                }
                m_uButtonState &= ~UISTATE_PUSHED;
                m_uButtonState &= ~UISTATE_SELECTED;
                m_ptMove.x = 0;
                m_ptMove.y = 0;
                if (NULL != m_pSelectRect)
                {
                    //Remove(m_pSelectRect);
                    m_pSelectRect->Delete();
                    NeedUpdate();
                    m_pSelectRect = NULL;
                }
            }
            return;
        }

        if (event.Type == UIEVENT_MOUSEMOVE)
        {
            do
            {
                if (!IsEnabled())
                    break;

                //auto left_button_down = (GetAsyncKeyState(VK_LBUTTON) & 0x8000);
                //if (!left_button_down)
                //{
                //    //debughelper::DebugPrint(0, "左键没有按下\n");
                //    break;
                //}

                ////if (!(m_uButtonState & UISTATE_PUSHED))
                ////{
                ////    debughelper::DebugPrint(0, "!(m_uButtonState & UISTATE_PUSHED)\n");
                ////    SelectItem(-1);
                ////    break;
                ////}

                //m_uButtonState |= UISTATE_SELECTED;//暂时用作 点击移动状态
                //m_ptMove = event.ptMouse;

                ////显示选择区域
                //RECT rc = { 0 };
                //rc.left = m_ptDown.x < m_ptMove.x ? m_ptDown.x : m_ptMove.x;
                //rc.top = m_ptDown.y < m_ptMove.y ? m_ptDown.y : m_ptMove.y;
                //rc.right = m_ptDown.x > m_ptMove.x ? m_ptDown.x : m_ptMove.x;
                //rc.bottom = m_ptDown.y > m_ptMove.y ? m_ptDown.y : m_ptMove.y;

                //if (NULL == m_pSelectRect)
                //{
                //    m_pSelectRect = new CControlUI;
                //    m_pSelectRect->SetFloat(true);
                //    //m_pSelectRect->SetBorderSize(1);
                //    //m_pSelectRect->SetBorderColor(0xFF000000);
                //    m_pSelectRect->SetBkColor(0x25000000);
                //    //m_pSelectRect->SetBkColor(0x25FFFFFF);
                //    //m_pSelectRect->SetAttribute("bkimage",
                //    //    "file='E:\\wan-yun-v3\\WinApp\\Release\\x64\\Res\\Background_Image\\123.bmp' fade='10'");
                //    //m_pSelectRect->SetManager(GetManager(), m_pList);
                //    GetManager()->InitControls(m_pSelectRect, this);
                //    NeedUpdate();
                //    bbbb = 1;
                //    m_pSelectRect->SetName("选择框");
                //    Add(m_pSelectRect);
                //}

                //CControlUI* pParent = m_pSelectRect->GetParent();
                //if (pParent != nullptr)
                //{
                //    const RECT rcParent = pParent->GetPos();
                //    rc.left -= rcParent.left;
                //    rc.right -= rcParent.left;
                //    rc.top -= rcParent.top;
                //    rc.bottom -= rcParent.top;
                //}

                //m_pSelectRect->SetPos(rc);

                ////选择选中的item
                //if (m_uButtonState & UISTATE_PUSHED && m_ptMove.x != 0 && m_ptMove.y != 0)
                //{
                //    RECT rc = { 0 };
                //    rc.left = m_ptDown.x < m_ptMove.x ? m_ptDown.x : m_ptMove.x;
                //    rc.top = m_ptDown.y < m_ptMove.y ? m_ptDown.y : m_ptMove.y;
                //    rc.right = m_ptDown.x > m_ptMove.x ? m_ptDown.x : m_ptMove.x;
                //    rc.bottom = m_ptDown.y > m_ptMove.y ? m_ptDown.y : m_ptMove.y;

                //    RECT rcItem = { 0 };
                //    RECT rcPos = { 0 };
                //    int iOldSel = m_iCurSel;
                //    CScrollBarUI* pVerScroll = GetVerticalScrollBar();
                //    if (pVerScroll == nullptr)
                //        return;
                //    int nPos = pVerScroll->GetScrollPos();
                //    int nItemHeight = 24;
                //    int nVisibleTop = nPos / nItemHeight;
                //    for (int i = nVisibleTop; i < GetCount(); i++)
                //    {
                //        auto pControl = GetItemAt(i);
                //        auto pItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
                //        if (NULL != pItem && pControl->IsEnabled() && pControl->IsVisible())
                //        {
                //            rcItem = pControl->GetPos();
                //            if (::IntersectRect(&rcPos, &rcItem, &rc))
                //            {
                //                pItem->Select(true, false);
                //                //debughelper::DebugPrint(0, "name = %s Select true, index = %d, nVisibleTop = %d\n", pControl->GetName().GetData(), i, nVisibleTop);
                //                m_iCurSel = i;
                //            }
                //            else
                //            {
                //                pItem->Select(false, false);
                //                //debughelper::DebugPrint(0, "name = %s Select false\n", pControl->GetName().GetData());
                //            }

                //            if (!::IntersectRect(&rcPos, &m_rcItem, &rcItem))
                //            {
                //                break;
                //            }
                //        }
                //    }
                //    //if (m_pManager != NULL && m_iCurSel != -1)
                //    //{
                //    //    m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMSELECT, m_iCurSel, iOldSel);
                //    //}
                //}


            } while (0);

            return;
        }

        CVerticalLayoutUI::DoEvent(event);
    }

    CListHeaderUI* CListUI::GetHeader() const
    {
        return m_pHeader;
    }

    CContainerUI* CListUI::GetList() const
    {
        return m_pList;
    }

    bool CListUI::GetScrollSelect()
    {
        return m_bScrollSelect;
    }

    void CListUI::SetScrollSelect(bool bScrollSelect)
    {
        m_bScrollSelect = bScrollSelect;
    }

    int CListUI::GetCurSel() const
    {
        return m_iCurSel;
    }

    unsigned int CListUI::GetSelectedCount() const
    {
        unsigned int result = 0;
        for (int loop = 0; loop < GetCount(); loop++)
        {
            IListItemUI* pListItem = static_cast<IListItemUI*>(GetItemAt(loop)->GetInterface(DUI_CTR_ILISTITEM));
            pListItem != nullptr && pListItem->IsSelected() == true ? result++ : 0;
        }
        return result;
    }

    bool CListUI::SelectItem(int new_selected_index, bool bTakeFocus, bool bTriggerEvent)
    {
        bool result = false;
        bool bCtrl = (GetKeyState(VK_CONTROL) & 0x8000);
        bool bShift = (GetKeyState(VK_SHIFT) & 0x8000);

        do
        {
            if (m_LockSelectedItem)
                break;

            // iIndex = -1 所有子元素设置为不选择
            if (new_selected_index == -1)
            {
                int current_index = 0;
                CControlUI* pControl = nullptr;
                IListItemUI* pListItem = nullptr;
                int count = GetCount();
                while (current_index < count)
                {
                    pControl = GetItemAt(current_index);
                    pListItem = 
                        pControl != nullptr ? 
                        static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM)) : nullptr;
                    pListItem ? pListItem->Select(false, false) : 0;
                    current_index++;
                }
                int iOldSel = m_iCurSel;
                m_iCurSel = -1;

                if (bTriggerEvent == true && m_pManager != nullptr)
                {
                    m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMSELECT, m_iCurSel, iOldSel);
                }
                result = true;
                break;
            }

            // iIndex != -1
            new_selected_index = new_selected_index > GetCount() ? GetCount() - 1 : new_selected_index;

            CControlUI* pSelectedControl = GetItemAt(new_selected_index);
            if (pSelectedControl == nullptr || pSelectedControl->IsVisible() == false || pSelectedControl->IsEnabled() == false)
                break;

            IListItemUI* pSelectedListItem = static_cast<IListItemUI*>(pSelectedControl->GetInterface(DUI_CTR_ILISTITEM));
            if (pSelectedListItem == nullptr)
                break;

            if (bCtrl == false)
            {
                for (int i = 0; i < GetCount(); i++)
                {
                    CControlUI* pControl = GetItemAt(i);
                    IListItemUI* pListItem = pControl != nullptr ? static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM)) : nullptr;

                    if (pListItem == nullptr)
                        continue;

                    if (i == new_selected_index && pListItem->IsSelected() == false)
                    {
                        pListItem->Select(true, true);
                    }
                    else if (i != new_selected_index && pListItem->IsSelected() == true && bShift == false)
                    {
                        pListItem->Select(false, false);
                    }
                }
            }

            if (new_selected_index == m_iCurSel)
            {
                result = true;
                break;
            }

            int iOldSel = m_iCurSel;
            m_iCurSel = new_selected_index;
            EnsureVisible(m_iCurSel);
            bTakeFocus == true ? pSelectedControl->SetFocus() : 0;

            CListElementUI* pCtrlToSetHot = static_cast<CListElementUI*>(pSelectedControl->GetInterface(DUI_CTR_LISTELEMENT));
            if (pCtrlToSetHot && pCtrlToSetHot->IsSelected() == true && pCtrlToSetHot->IsHot() == false)
            {
                pCtrlToSetHot->Hot(true);
            }
            if (bTriggerEvent && m_pManager != nullptr)
            {
                m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMSELECT, m_iCurSel, iOldSel);
            }
            result = true;

        } while (0);

        return result;
    }

    bool CListUI::SelectRange(int iIndex, bool bTakeFocus)
    {
        int i = 0;
        int iFirst = m_iCurSel;
        int iLast = iIndex;
        int iCount = GetCount();

        if (iFirst == iLast) return true;

        CControlUI* pControl = GetItemAt(iIndex);
        if (pControl == NULL) return false;
        if (!pControl->IsVisible()) return false;
        if (!pControl->IsEnabled()) return false;

        IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
        if (pListItem == NULL) return false;
        if (!pListItem->Select(true, false))
        {
            m_iCurSel = -1;
            return false;
        }
        EnsureVisible(iIndex);
        if (bTakeFocus) pControl->SetFocus();
        if (m_pManager != NULL)
        {
            m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMSELECT, iIndex, m_iCurSel);
        }
        //locate (and select) either first or last
        // (so order is arbitrary)
        while (i < iCount)
        {
            if (i == iFirst || i == iLast)
            {
                i++;
                break;
            }

            CControlUI* pControl = GetItemAt(i);
            if (pControl != NULL)
            {
                IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
                if (pListItem != NULL) pListItem->Select(false, false);
            }
            i++;
        }

        // select rest of range
        while (i < iCount)
        {
            CControlUI* pControl = GetItemAt(i);
            if (pControl != NULL)
            {
                IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
                if (pListItem != NULL) pListItem->Select(true, false);
            }
            if (i == iFirst || i == iLast)
            {
                i++;
                break;
            }
            i++;
        }

        // unselect rest of range
        while (i < iCount)
        {
            CControlUI* pControl = GetItemAt(i);
            if (pControl != NULL)
            {
                IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
                if (pListItem != NULL) pListItem->Select(false, false);
            }
            i++;
        }

        return true;
    }

    TListInfoUI* CListUI::GetListInfo()
    {
        return &m_ListInfo;
    }

    int CListUI::GetChildPadding() const
    {
        return m_pList->GetChildPadding();
    }

    void CListUI::SetChildPadding(int iPadding)
    {
        m_pList->SetChildPadding(iPadding);
    }

    UINT CListUI::GetItemFixedHeight()
    {
        return m_ListInfo.uFixedHeight;
    }

    void CListUI::SetItemFixedHeight(UINT nHeight)
    {
        m_ListInfo.uFixedHeight = nHeight;
        NeedUpdate();
    }

    int CListUI::GetItemFont(int index)
    {
        return m_ListInfo.nFont;
    }

    void CListUI::SetItemFont(int index)
    {
        m_ListInfo.nFont = index;
        NeedUpdate();
    }

    UINT CListUI::GetItemTextStyle()
    {
        return m_ListInfo.uTextStyle;
    }

    void CListUI::SetItemTextStyle(UINT uStyle)
    {
        m_ListInfo.uTextStyle = uStyle;
        NeedUpdate();
    }

    void CListUI::SetItemTextPadding(RECT rc)
    {
        m_ListInfo.rcTextPadding = rc;
        NeedUpdate();
    }

    RECT CListUI::GetItemTextPadding() const
    {
        return m_ListInfo.rcTextPadding;
    }

    void CListUI::SetItemTextColor(DWORD dwTextColor)
    {
        m_ListInfo.dwTextColor = dwTextColor;
        Invalidate();
    }

    void CListUI::SetItemBkColor(DWORD dwBkColor)
    {
        m_ListInfo.dwBkColor = dwBkColor;
        Invalidate();
    }

    void CListUI::SetItemBkImage(LPCTSTR pStrImage)
    {
        if (m_ListInfo.diBk.sDrawString == pStrImage && m_ListInfo.diBk.pImageInfo != NULL) return;
        m_ListInfo.diBk.Clear();
        m_ListInfo.diBk.sDrawString = pStrImage;
        Invalidate();
    }

    bool CListUI::IsAlternateBk() const
    {
        return m_ListInfo.bAlternateBk;
    }

    void CListUI::SetAlternateBk(bool bAlternateBk)
    {
        m_ListInfo.bAlternateBk = bAlternateBk;
        Invalidate();
    }

    DWORD CListUI::GetItemTextColor() const
    {
        return m_ListInfo.dwTextColor;
    }

    DWORD CListUI::GetItemBkColor() const
    {
        return m_ListInfo.dwBkColor;
    }

    LPCTSTR CListUI::GetItemBkImage() const
    {
        return m_ListInfo.diBk.sDrawString;
    }

    void CListUI::SetSelectedItemTextColor(DWORD dwTextColor)
    {
        m_ListInfo.dwSelectedTextColor = dwTextColor;
        Invalidate();
    }

    void CListUI::SetSelectedItemBkColor(DWORD dwBkColor)
    {
        m_ListInfo.dwSelectedBkColor = dwBkColor;
        Invalidate();
    }

    void CListUI::SetSelectedItemImage(LPCTSTR pStrImage)
    {
        if (m_ListInfo.diSelected.sDrawString == pStrImage && m_ListInfo.diSelected.pImageInfo != NULL) return;
        m_ListInfo.diSelected.Clear();
        m_ListInfo.diSelected.sDrawString = pStrImage;
        Invalidate();
    }

    DWORD CListUI::GetSelectedItemTextColor() const
    {
        return m_ListInfo.dwSelectedTextColor;
    }

    DWORD CListUI::GetSelectedItemBkColor() const
    {
        return m_ListInfo.dwSelectedBkColor;
    }

    LPCTSTR CListUI::GetSelectedItemImage() const
    {
        return m_ListInfo.diSelected.sDrawString;
    }

    void CListUI::SetHotItemTextColor(DWORD dwTextColor)
    {
        m_ListInfo.dwHotTextColor = dwTextColor;
        Invalidate();
    }

    void CListUI::SetHotItemBkColor(DWORD dwBkColor)
    {
        m_ListInfo.dwHotBkColor = dwBkColor;
        Invalidate();
    }

    void CListUI::SetHotItemImage(LPCTSTR pStrImage)
    {
        if (m_ListInfo.diHot.sDrawString == pStrImage && m_ListInfo.diHot.pImageInfo != NULL) return;
        m_ListInfo.diHot.Clear();
        m_ListInfo.diHot.sDrawString = pStrImage;
        Invalidate();
    }

    DWORD CListUI::GetHotItemTextColor() const
    {
        return m_ListInfo.dwHotTextColor;
    }
    DWORD CListUI::GetHotItemBkColor() const
    {
        return m_ListInfo.dwHotBkColor;
    }

    LPCTSTR CListUI::GetHotItemImage() const
    {
        return m_ListInfo.diHot.sDrawString;
    }

    void CListUI::SetDisabledItemTextColor(DWORD dwTextColor)
    {
        m_ListInfo.dwDisabledTextColor = dwTextColor;
        Invalidate();
    }

    void CListUI::SetDisabledItemBkColor(DWORD dwBkColor)
    {
        m_ListInfo.dwDisabledBkColor = dwBkColor;
        Invalidate();
    }

    void CListUI::SetDisabledItemImage(LPCTSTR pStrImage)
    {
        if (m_ListInfo.diDisabled.sDrawString == pStrImage && m_ListInfo.diDisabled.pImageInfo != NULL) return;
        m_ListInfo.diDisabled.Clear();
        m_ListInfo.diDisabled.sDrawString = pStrImage;
        Invalidate();
    }

    DWORD CListUI::GetDisabledItemTextColor() const
    {
        return m_ListInfo.dwDisabledTextColor;
    }

    DWORD CListUI::GetDisabledItemBkColor() const
    {
        return m_ListInfo.dwDisabledBkColor;
    }

    LPCTSTR CListUI::GetDisabledItemImage() const
    {
        return m_ListInfo.diDisabled.sDrawString;
    }

    int CListUI::GetItemHLineSize() const
    {
        return m_ListInfo.iHLineSize;
    }

    void CListUI::SetItemHLineSize(int iSize)
    {
        m_ListInfo.iHLineSize = iSize;
        Invalidate();
    }

    DWORD CListUI::GetItemHLineColor() const
    {
        return m_ListInfo.dwHLineColor;
    }

    void CListUI::SetItemHLineColor(DWORD dwLineColor)
    {
        m_ListInfo.dwHLineColor = dwLineColor;
        Invalidate();
    }

    int CListUI::GetItemVLineSize() const
    {
        return m_ListInfo.iVLineSize;
    }

    void CListUI::SetItemVLineSize(int iSize)
    {
        m_ListInfo.iVLineSize = iSize;
        Invalidate();
    }

    DWORD CListUI::GetItemVLineColor() const
    {
        return m_ListInfo.dwVLineColor;
    }

    void CListUI::SetItemVLineColor(DWORD dwLineColor)
    {
        m_ListInfo.dwVLineColor = dwLineColor;
        Invalidate();
    }

    bool CListUI::IsItemShowHtml()
    {
        return m_ListInfo.bShowHtml;
    }

    void CListUI::SetItemShowHtml(bool bShowHtml)
    {
        if (m_ListInfo.bShowHtml == bShowHtml) return;

        m_ListInfo.bShowHtml = bShowHtml;
        NeedUpdate();
    }

    void CListUI::SetMultiExpanding(bool bMultiExpandable)
    {
        m_ListInfo.bMultiExpandable = bMultiExpandable;
    }

    bool CListUI::ExpandItem(int iIndex, bool bExpand /*= true*/)
    {
        if (m_iExpandedItem >= 0 && !m_ListInfo.bMultiExpandable)
        {
            CControlUI* pControl = GetItemAt(m_iExpandedItem);
            if (pControl != NULL)
            {
                IListItemUI* pItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
                if (pItem != NULL) pItem->Expand(false);
            }
            m_iExpandedItem = -1;
        }
        if (bExpand)
        {
            CControlUI* pControl = GetItemAt(iIndex);
            if (pControl == NULL) return false;
            if (!pControl->IsVisible()) return false;
            IListItemUI* pItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
            if (pItem == NULL) return false;
            m_iExpandedItem = iIndex;
            if (!pItem->Expand(true))
            {
                m_iExpandedItem = -1;
                return false;
            }
        }
        NeedUpdate();
        return true;
    }

    int CListUI::GetExpandedItem() const
    {
        return m_iExpandedItem;
    }

    void CListUI::EnsureVisible(int iIndex)
    {
        if (m_iCurSel < 0) return;
        RECT rcItem = m_pList->GetItemAt(iIndex)->GetPos();
        RECT rcList = m_pList->GetPos();
        RECT rcListInset = m_pList->GetInset();

        rcList.left += rcListInset.left;
        rcList.top += rcListInset.top;
        rcList.right -= rcListInset.right;
        rcList.bottom -= rcListInset.bottom;

        CScrollBarUI* pHorizontalScrollBar = m_pList->GetHorizontalScrollBar();
        if (pHorizontalScrollBar && pHorizontalScrollBar->IsVisible()) rcList.bottom -= pHorizontalScrollBar->GetFixedHeight();

        int iPos = m_pList->GetScrollPos().cy;
        if (rcItem.top >= rcList.top && rcItem.bottom < rcList.bottom) return;
        int dx = 0;
        if (rcItem.top < rcList.top) dx = rcItem.top - rcList.top;
        if (rcItem.bottom > rcList.bottom) dx = rcItem.bottom - rcList.bottom;
        Scroll(0, dx);
    }

    void CListUI::Scroll(int dx, int dy)
    {
        if (dx == 0 && dy == 0) return;
        SIZE sz = m_pList->GetScrollPos();
        m_pList->SetScrollPos(CDuiSize(sz.cx + dx, sz.cy + dy));
    }

    void CListUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
    {
        if (_tcscmp(pstrName, _T("header")) == 0) GetHeader()->SetVisible(_tcscmp(pstrValue, _T("hidden")) != 0);
        else if (_tcscmp(pstrName, _T("headerbkimage")) == 0) GetHeader()->SetBkImage(pstrValue);
        else if (_tcscmp(pstrName, _T("scrollselect")) == 0) SetScrollSelect(_tcscmp(pstrValue, _T("true")) == 0);
        else if (_tcscmp(pstrName, _T("multiexpanding")) == 0) SetMultiExpanding(_tcscmp(pstrValue, _T("true")) == 0);
        else if (_tcscmp(pstrName, _T("itemheight")) == 0) m_ListInfo.uFixedHeight = _ttoi(pstrValue);
        else if (_tcscmp(pstrName, _T("itemfont")) == 0) m_ListInfo.nFont = _ttoi(pstrValue);
        else if (_tcscmp(pstrName, _T("itemalign")) == 0)
        {
            if (_tcsstr(pstrValue, _T("left")) != NULL)
            {
                m_ListInfo.uTextStyle &= ~(DT_CENTER | DT_RIGHT);
                m_ListInfo.uTextStyle |= DT_LEFT;
            }
            if (_tcsstr(pstrValue, _T("center")) != NULL)
            {
                m_ListInfo.uTextStyle &= ~(DT_LEFT | DT_RIGHT);
                m_ListInfo.uTextStyle |= DT_CENTER;
            }
            if (_tcsstr(pstrValue, _T("right")) != NULL)
            {
                m_ListInfo.uTextStyle &= ~(DT_LEFT | DT_CENTER);
                m_ListInfo.uTextStyle |= DT_RIGHT;
            }
        }
        else if (_tcscmp(pstrName, _T("itemvalign")) == 0)
        {
            if (_tcsstr(pstrValue, _T("top")) != NULL)
            {
                m_ListInfo.uTextStyle &= ~(DT_BOTTOM | DT_VCENTER);
                m_ListInfo.uTextStyle |= DT_TOP;
            }
            if (_tcsstr(pstrValue, _T("vcenter")) != NULL)
            {
                m_ListInfo.uTextStyle &= ~(DT_TOP | DT_BOTTOM);
                m_ListInfo.uTextStyle |= DT_VCENTER;
            }
            if (_tcsstr(pstrValue, _T("bottom")) != NULL)
            {
                m_ListInfo.uTextStyle &= ~(DT_TOP | DT_VCENTER);
                m_ListInfo.uTextStyle |= DT_BOTTOM;
            }
        }
        else if (_tcscmp(pstrName, _T("itemendellipsis")) == 0)
        {
            if (_tcscmp(pstrValue, _T("true")) == 0) m_ListInfo.uTextStyle |= DT_END_ELLIPSIS;
            else m_ListInfo.uTextStyle &= ~DT_END_ELLIPSIS;
        }
        else if (_tcscmp(pstrName, _T("itemmultiline")) == 0)
        {
            if (_tcscmp(pstrValue, _T("true")) == 0)
            {
                m_ListInfo.uTextStyle &= ~DT_SINGLELINE;
                m_ListInfo.uTextStyle |= DT_WORDBREAK;
            }
            else m_ListInfo.uTextStyle |= DT_SINGLELINE;
        }
        else if (_tcscmp(pstrName, _T("itemtextpadding")) == 0)
        {
            RECT rcTextPadding = { 0 };
            LPTSTR pstr = NULL;
            rcTextPadding.left = _tcstol(pstrValue, &pstr, 10);  ASSERT(pstr);
            rcTextPadding.top = _tcstol(pstr + 1, &pstr, 10);    ASSERT(pstr);
            rcTextPadding.right = _tcstol(pstr + 1, &pstr, 10);  ASSERT(pstr);
            rcTextPadding.bottom = _tcstol(pstr + 1, &pstr, 10); ASSERT(pstr);
            SetItemTextPadding(rcTextPadding);
        }
        else if (_tcscmp(pstrName, _T("itemtextcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetItemTextColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itembkcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetItemBkColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itembkimage")) == 0) SetItemBkImage(pstrValue);
        else if (_tcscmp(pstrName, _T("itemaltbk")) == 0) SetAlternateBk(_tcscmp(pstrValue, _T("true")) == 0);
        else if (_tcscmp(pstrName, _T("itemselectedtextcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetSelectedItemTextColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemselectedbkcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetSelectedItemBkColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemselectedimage")) == 0) SetSelectedItemImage(pstrValue);
        else if (_tcscmp(pstrName, _T("itemhottextcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetHotItemTextColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemhotbkcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetHotItemBkColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemhotimage")) == 0) SetHotItemImage(pstrValue);
        else if (_tcscmp(pstrName, _T("itemdisabledtextcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetDisabledItemTextColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemdisabledbkcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetDisabledItemBkColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemdisabledimage")) == 0) SetDisabledItemImage(pstrValue);
        else if (_tcscmp(pstrName, _T("itemvlinesize")) == 0)
        {
            SetItemVLineSize(_ttoi(pstrValue));
        }
        else if (_tcscmp(pstrName, _T("itemvlinecolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetItemVLineColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemhlinesize")) == 0)
        {
            SetItemHLineSize(_ttoi(pstrValue));
        }
        else if (_tcscmp(pstrName, _T("itemhlinecolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetItemHLineColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("itemshowhtml")) == 0) SetItemShowHtml(_tcscmp(pstrValue, _T("true")) == 0);
        else CVerticalLayoutUI::SetAttribute(pstrName, pstrValue);
    }

    IListCallbackUI* CListUI::GetTextCallback() const
    {
        return m_pCallback;
    }

    void CListUI::SetTextCallback(IListCallbackUI* pCallback)
    {
        m_pCallback = pCallback;
    }

    SIZE CListUI::GetScrollPos() const
    {
        return m_pList->GetScrollPos();
    }

    SIZE CListUI::GetScrollRange() const
    {
        return m_pList->GetScrollRange();
    }

    void CListUI::SetScrollPos(SIZE szPos)
    {
        m_pList->SetScrollPos(szPos);
    }

    void CListUI::LineUp()
    {
        m_pList->LineUp();
    }

    void CListUI::LineDown()
    {
        m_pList->LineDown();
    }

    void CListUI::PageUp()
    {
        m_pList->PageUp();
    }

    void CListUI::PageDown()
    {
        m_pList->PageDown();
    }

    void CListUI::HomeUp()
    {
        m_pList->HomeUp();
    }

    void CListUI::EndDown()
    {
        m_pList->EndDown();
    }

    void CListUI::LineLeft()
    {
        m_pList->LineLeft();
    }

    void CListUI::LineRight()
    {
        m_pList->LineRight();
    }

    void CListUI::PageLeft()
    {
        m_pList->PageLeft();
    }

    void CListUI::PageRight()
    {
        m_pList->PageRight();
    }

    void CListUI::HomeLeft()
    {
        m_pList->HomeLeft();
    }

    void CListUI::EndRight()
    {
        m_pList->EndRight();
    }

    void CListUI::EnableScrollBar(bool bEnableVertical, bool bEnableHorizontal)
    {
        m_pList->EnableScrollBar(bEnableVertical, bEnableHorizontal);
    }

    CScrollBarUI* CListUI::GetVerticalScrollBar() const
    {
        return m_pList->GetVerticalScrollBar();
    }

    CScrollBarUI* CListUI::GetHorizontalScrollBar() const
    {
        return m_pList->GetHorizontalScrollBar();
    }

    bool CListUI::SortItems(PULVCompareFunc pfnCompare, UINT_PTR dwData)
    {
        if (!m_pList) 
            return false;
        int iCurSel = m_iCurSel;
        bool bResult = m_pList->SortItems(pfnCompare, dwData, iCurSel);
        if (bResult)
        {
            m_iCurSel = iCurSel;
            EnsureVisible(m_iCurSel);
            NeedUpdate();
        }
        return bResult;
    }

    bool CListUI::IsSubControl(CControlUI* pControl)
    {
        bool result = true;
        do
        {
            if (this->GetItemIndex(pControl) != -1)
                goto ReturnTrue;

            for (int it = m_items.GetSize() - 1; it >= 0; it--)
            {
                if (pControl == static_cast<CControlUI*>(m_items[it]))
                    goto ReturnTrue;
            }

            result = false;
        } while (0);

ReturnTrue:
        return result;
    }

    void CListUI::SetLockSelectedItem(bool lock)
    {
        m_LockSelectedItem = lock;
    }

    bool CListUI::GetIsLockSelectedItem() const
    {
        return m_LockSelectedItem;
    }

    DuiLib::CControlUI* CListUI::GetMouseSelectRect() const
    {
        return m_pSelectRect;
    }

    void CListUI::SetName(LPCTSTR pstrName)
    {
        DuiLib::CDuiString temp(pstrName);
        temp.Append(_T("_ListBody"));
        m_pList->SetName(temp.GetData());
        m_sName.Assign(pstrName);
        AfterSetNameCall();
    }

    int CListUI::GetColumnCount() const
    {
        return m_ListInfo.nColumns;
        //return m_pHeader->GetCount();
    }

    void CListUI::GetWholeItem(std::vector<CControlUI*>& out)
    {
        m_pList->GetAllItems(out);
    }

    void CListUI::ResetWholeItemIndex(const std::vector<CControlUI*>& in)
    {
        m_pList->ResetAllItemIndex(in);
    }

    bool CListUI::SelectItemWhenCtrlKeyDown(int dest_control_index, bool bTakeFocus /*= false*/, bool bTriggerEvent /*= true*/)
    {
        bool result = false;
        do
        {
            bool ctrl_key_down = (GetKeyState(VK_CONTROL) & 0x8000);
            if (!ctrl_key_down)
                break;

            auto total_count = GetCount();
            if (total_count < 0 || dest_control_index >= total_count)
                break;

            CControlUI* pControl = GetItemAt(dest_control_index);
            IListItemUI* pListItem = pControl != nullptr ?
                static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM)) : nullptr;

            if (pListItem == nullptr)
                break;

            pListItem->Select(true, false);
            // 考虑新增一个 CListUI::UnSelectItem(m_iIndex);
            //if (i == new_selected_index && pListItem->IsSelected() == false)
            //{
            //    pListItem->Select(true, true);
            //}
            //else if (i != new_selected_index && pListItem->IsSelected() == true && bShift == false)
            //{
            //    pListItem->Select(false, false);
            //}

        } while (0);
        return result;

    }

    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListBodyUI::CListBodyUI(CListUI* pOwner) : m_pOwner(pOwner), m_pCompareFunc(nullptr), m_compareData(0)
    {
        ASSERT(m_pOwner);
    }

    LPCTSTR CListBodyUI::GetClass() const
    {
        return _T("ListBody");
    }

    bool CListBodyUI::SortItems(PULVCompareFunc pfnCompare, UINT_PTR dwData, int& iCurSel)
    {
        if (!pfnCompare) 
            return false;

        m_pCompareFunc = pfnCompare;
        m_compareData = dwData;
        CControlUI* pCurSelControl = GetItemAt(iCurSel);
        CControlUI** pData = (CControlUI**)m_items.GetData();
        qsort_s(m_items.GetData(), m_items.GetSize(), sizeof(CControlUI*), CListBodyUI::ItemComareFunc, this);
        if (pCurSelControl) 
            iCurSel = GetItemIndex(pCurSelControl);
        IListItemUI* pItem = NULL;
        CControlUI* pItemContrl = nullptr;
        for (int i = 0; i < m_items.GetSize(); ++i)
        {
            pItemContrl = static_cast<CControlUI*>(m_items[i]);
            pItem = dynamic_cast<IListItemUI*>(pItemContrl);
            if (pItem != nullptr)
            {
                pItem->SetIndex(i);
            }
        }

        return true;
    }

    void CListBodyUI::SetEnabled(bool new_status)
    {
        if (m_pHorizontalScrollBar && new_status == false)
        {
            m_pHorizontalScrollBar->SetEnabled(false);
        }
        if (m_pVerticalScrollBar && new_status == false)
        {
            m_pVerticalScrollBar->SetEnabled(false);
        }
        CControlUI::SetEnabled(new_status);
    }


void CListBodyUI::GetAllItems(std::vector<CControlUI*>& out)
{
    int loop = m_items.GetSize();
    CControlUI* p = nullptr;
    for (int i = 0; loop > 0 && i < loop; i++)
    {
        if (m_items.GetAt(i))
        {
            p = static_cast<CControlUI*>(m_items.GetAt(i));
            out.push_back(p);
        }
    }
}

void CListBodyUI::ResetAllItemIndex(const std::vector<CControlUI*>& in)
{
    int loop = m_items.GetSize();
    if (loop != in.size())
        return;
    loop = 0;
    for (auto p : in)
        m_items.SetAt(loop++, p);
}

int __cdecl CListBodyUI::ItemComareFunc(void* pvlocale, const void* item1, const void* item2)
    {
        CListBodyUI* pThis = (CListBodyUI*)pvlocale;
        if (!pThis || !item1 || !item2)
            return 0;
        return pThis->ItemComareFunc(item1, item2);
    }

    int __cdecl CListBodyUI::ItemComareFunc(const void* item1, const void* item2)
    {
        CControlUI* pControl1 = *(CControlUI**)item1;
        CControlUI* pControl2 = *(CControlUI**)item2;
        return m_pCompareFunc((UINT_PTR)pControl1, (UINT_PTR)pControl2, m_compareData);
    }

    void CListBodyUI::SetScrollPos(SIZE szPos)
    {
        int cx = 0;
        int cy = 0;
        if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible())
        {
            int iLastScrollPos = m_pVerticalScrollBar->GetScrollPos();
            m_pVerticalScrollBar->SetScrollPos(szPos.cy);
            cy = m_pVerticalScrollBar->GetScrollPos() - iLastScrollPos;
        }

        if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible())
        {
            int iLastScrollPos = m_pHorizontalScrollBar->GetScrollPos();
            m_pHorizontalScrollBar->SetScrollPos(szPos.cx);
            cx = m_pHorizontalScrollBar->GetScrollPos() - iLastScrollPos;
        }

        if (cx == 0 && cy == 0) return;

        for (int it2 = 0; it2 < m_items.GetSize(); it2++)
        {
            CControlUI* pControl = static_cast<CControlUI*>(m_items[it2]);
            if (!pControl->IsVisible()) continue;
            if (pControl->IsFloat()) continue;
            pControl->Move(CDuiSize(-cx, -cy), false);
        }

        Invalidate();

        if (cx != 0 && m_pOwner)
        {
            CListHeaderUI* pHeader = m_pOwner->GetHeader();
            if (pHeader == NULL) return;
            TListInfoUI* pInfo = m_pOwner->GetListInfo();
            pInfo->nColumns = MIN(pHeader->GetCount(), UILIST_MAX_COLUMNS);
            for (int i = 0; i < pInfo->nColumns; i++)
            {
                CControlUI* pControl = static_cast<CControlUI*>(pHeader->GetItemAt(i));
                if (!pControl->IsVisible()) continue;
                if (pControl->IsFloat()) continue;
                pControl->Move(CDuiSize(-cx, -cy), false);
                pInfo->rcColumn[i] = pControl->GetPos();
            }
            pHeader->Invalidate();
        }
    }

    void CListBodyUI::SetPos(RECT rc, bool bNeedInvalidate)
    {
        CControlUI::SetPos(rc, bNeedInvalidate);
        rc = m_rcItem;

        // Adjust for inset
        rc.left += m_rcInset.left;
        rc.top += m_rcInset.top;
        rc.right -= m_rcInset.right;
        rc.bottom -= m_rcInset.bottom;
        if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible()) rc.right -= m_pVerticalScrollBar->GetFixedWidth();
        if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible()) rc.bottom -= m_pHorizontalScrollBar->GetFixedHeight();

        // Determine the minimum size
        SIZE szAvailable = { rc.right - rc.left, rc.bottom - rc.top };
        if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible())
            szAvailable.cx += m_pHorizontalScrollBar->GetScrollRange();

        int iChildPadding = m_iChildPadding;
        TListInfoUI* pInfo = NULL;
        if (m_pOwner)
        {
            pInfo = m_pOwner->GetListInfo();
            if (pInfo != NULL)
            {
                iChildPadding += pInfo->iHLineSize;
                if (pInfo->nColumns > 0)
                {
                    szAvailable.cx = pInfo->rcColumn[pInfo->nColumns - 1].right - pInfo->rcColumn[0].left;
                }
            }
        }

        int cxNeeded = 0;
        int cyFixed = 0;
        int nEstimateNum = 0;
        SIZE szControlAvailable;
        int iControlMaxWidth = 0;
        int iControlMaxHeight = 0;
        CControlUI* pControl = nullptr;
        for (int it1 = 0; it1 < m_items.GetSize(); it1++)
        {
            pControl = nullptr;
            pControl = static_cast<CControlUI*>(m_items[it1]);
            if (!pControl->IsVisible()) continue;
            if (pControl->IsFloat()) continue;
            szControlAvailable = szAvailable;
            RECT rcPadding = pControl->GetPadding();
            szControlAvailable.cx -= rcPadding.left + rcPadding.right;
            iControlMaxWidth = pControl->GetFixedWidth();
            iControlMaxHeight = pControl->GetFixedHeight();
            if (iControlMaxWidth <= 0)
                iControlMaxWidth = pControl->GetMaxWidth();

            if (iControlMaxHeight <= 0)
                iControlMaxHeight = pControl->GetMaxHeight();

            if (szControlAvailable.cx > iControlMaxWidth)
                szControlAvailable.cx = iControlMaxWidth;

            if (szControlAvailable.cy > iControlMaxHeight)
                szControlAvailable.cy = iControlMaxHeight;

            SIZE sz = pControl->EstimateSize(szAvailable);

            if (sz.cy < pControl->GetMinHeight())
                sz.cy = pControl->GetMinHeight();

            if (sz.cy > pControl->GetMaxHeight())
                sz.cy = pControl->GetMaxHeight();

            cyFixed += sz.cy + pControl->GetPadding().top + pControl->GetPadding().bottom;

            sz.cx = MAX(sz.cx, 0);
            if (sz.cx < pControl->GetMinWidth())
                sz.cx = pControl->GetMinWidth();

            if (sz.cx > pControl->GetMaxWidth())
                sz.cx = pControl->GetMaxWidth();
            cxNeeded = MAX(cxNeeded, sz.cx);
            nEstimateNum++;
        }
        cyFixed += (nEstimateNum - 1) * iChildPadding;

        if (m_pOwner)
        {
            CListHeaderUI* pHeader = m_pOwner->GetHeader();
            if (pHeader != NULL && pHeader->GetCount() > 0)
            {
                cxNeeded = MAX(0, pHeader->EstimateSize(CDuiSize(rc.right - rc.left, rc.bottom - rc.top)).cx);
            }
        }

        // Place elements
        int cyNeeded = 0;
        // Position the elements
        SIZE szRemaining = szAvailable;
        int iPosY = rc.top;
        if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible())
        {
            iPosY -= m_pVerticalScrollBar->GetScrollPos();
        }
        int iPosX = rc.left;
        if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible())
        {
            iPosX -= m_pHorizontalScrollBar->GetScrollPos();
        }

        int iAdjustable = 0;
        for (int it2 = 0; it2 < m_items.GetSize(); it2++)
        {
            CControlUI* pControl = static_cast<CControlUI*>(m_items[it2]);
            if (!pControl->IsVisible()) continue;
            if (pControl->IsFloat())
            {
                SetFloatPos(it2);
                continue;
            }

            RECT rcPadding = pControl->GetPadding();
            szRemaining.cy -= rcPadding.top;
            szControlAvailable = szRemaining;
            szControlAvailable.cx -= rcPadding.left + rcPadding.right;
            iControlMaxWidth = pControl->GetFixedWidth();
            iControlMaxHeight = pControl->GetFixedHeight();
            if (iControlMaxWidth <= 0) iControlMaxWidth = pControl->GetMaxWidth();
            if (iControlMaxHeight <= 0) iControlMaxHeight = pControl->GetMaxHeight();
            if (szControlAvailable.cx > iControlMaxWidth) szControlAvailable.cx = iControlMaxWidth;
            if (szControlAvailable.cy > iControlMaxHeight) szControlAvailable.cy = iControlMaxHeight;
            SIZE sz = pControl->EstimateSize(szControlAvailable);
            if (sz.cy < pControl->GetMinHeight()) sz.cy = pControl->GetMinHeight();
            if (sz.cy > pControl->GetMaxHeight()) sz.cy = pControl->GetMaxHeight();
            sz.cx = pControl->GetMaxWidth();
            if (sz.cx == 0) sz.cx = szAvailable.cx - rcPadding.left - rcPadding.right;
            if (sz.cx < 0) sz.cx = 0;
            if (sz.cx > szControlAvailable.cx) sz.cx = szControlAvailable.cx;
            if (sz.cx < pControl->GetMinWidth()) sz.cx = pControl->GetMinWidth();

            RECT rcCtrl = { iPosX + rcPadding.left, iPosY + rcPadding.top, iPosX + rcPadding.left + sz.cx, iPosY + sz.cy + rcPadding.top + rcPadding.bottom };
            pControl->SetPos(rcCtrl, false);

            iPosY += sz.cy + iChildPadding + rcPadding.top + rcPadding.bottom;
            cyNeeded += sz.cy + rcPadding.top + rcPadding.bottom;
            szRemaining.cy -= sz.cy + iChildPadding + rcPadding.bottom;
        }
        cyNeeded += (nEstimateNum - 1) * iChildPadding;

        // Process the scrollbar
        ProcessScrollBar(rc, cxNeeded, cyNeeded);
    }

    void CListBodyUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pOwner != NULL) m_pOwner->DoEvent(event);
            else CControlUI::DoEvent(event);
            return;
        }

        if (m_pOwner != NULL)
        {
            if (event.Type == UIEVENT_SCROLLWHEEL)
            {
                if (m_pHorizontalScrollBar != NULL && m_pHorizontalScrollBar->IsVisible() && m_pHorizontalScrollBar->IsEnabled())
                {
                    RECT rcHorizontalScrollBar = m_pHorizontalScrollBar->GetPos();
                    if (::PtInRect(&rcHorizontalScrollBar, event.ptMouse))
                    {
                        switch (LOWORD(event.wParam))
                        {
                        case SB_LINEUP:
                            m_pOwner->LineLeft();
                            return;
                        case SB_LINEDOWN:
                            m_pOwner->LineRight();
                            return;
                        }
                    }
                }
            }
            m_pOwner->DoEvent(event);
        }
        else
        {
            CControlUI::DoEvent(event);
        }
    }

    bool CListBodyUI::DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl)
    {
        RECT rcTemp = { 0 };
        if (!::IntersectRect(&rcTemp, &rcPaint, &m_rcItem)) return true;

        TListInfoUI* pListInfo = NULL;
        if (m_pOwner) pListInfo = m_pOwner->GetListInfo();

        CRenderClip clip;
        CRenderClip::GenerateClip(hDC, rcTemp, clip);
        CControlUI::DoPaint(hDC, rcPaint, pStopControl);

        auto item_size = m_items.GetSize();
        if (item_size > 0)
        {
            RECT rc = m_rcItem;
            rc.left += m_rcInset.left;
            rc.top += m_rcInset.top;
            rc.right -= m_rcInset.right;
            rc.bottom -= m_rcInset.bottom;

            if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible())
                rc.right -= m_pVerticalScrollBar->GetFixedWidth();

            if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible())
                rc.bottom -= m_pHorizontalScrollBar->GetFixedHeight();

            if (!::IntersectRect(&rcTemp, &rcPaint, &rc))
            {
                auto loop1 = m_items.GetSize();
                for (int it = 0; it < loop1; it++)
                {
                    CControlUI* pControl = static_cast<CControlUI*>(m_items[it]);
                    if (pControl == pStopControl) return false;
                    if (!pControl->IsVisible()) continue;
                    if (!::IntersectRect(&rcTemp, &rcPaint, &pControl->GetPos())) continue;
                    if (pControl->IsFloat())
                    {
                        if (!::IntersectRect(&rcTemp, &m_rcItem, &pControl->GetPos())) continue;
                        if (!pControl->Paint(hDC, rcPaint, pStopControl)) return false;
                    }
                }
            }
            else
            {
                int iDrawIndex = 0;
                CRenderClip childClip;
                CRenderClip::GenerateClip(hDC, rcTemp, childClip);
                auto loop2 = m_items.GetSize();
                CControlUI* pControl = nullptr;
                for (int it = 0; it < loop2; it++)
                {
                    pControl = nullptr;
                    pControl = static_cast<CControlUI*>(m_items[it]);
                    if (pControl == pStopControl) 
                        return false;
                    if (!pControl->IsVisible())
                        continue;
                    if (!pControl->IsFloat())
                    {
                        IListItemUI* pListItem = static_cast<IListItemUI*>(pControl->GetInterface(DUI_CTR_ILISTITEM));
                        if (pListItem != NULL)
                        {
                            pListItem->SetDrawIndex(iDrawIndex);
                            iDrawIndex += 1;
                        }
                        if (pListInfo && pListInfo->iHLineSize > 0)
                        {
                            // 因为没有为最后一个预留分割条长度，如果list铺满，最后一条不会显示
                            RECT rcPadding = pControl->GetPadding();
                            const RECT& rcPos = pControl->GetPos();
                            RECT rcBottomLine = { rcPos.left, rcPos.bottom + rcPadding.bottom, rcPos.right, rcPos.bottom + rcPadding.bottom + pListInfo->iHLineSize };
                            if (::IntersectRect(&rcTemp, &rcPaint, &rcBottomLine))
                            {
                                rcBottomLine.top += pListInfo->iHLineSize / 2;
                                rcBottomLine.bottom = rcBottomLine.top;
                                CRenderEngine::DrawLine(hDC, rcBottomLine, pListInfo->iHLineSize, GetAdjustColor(pListInfo->dwHLineColor));
                            }
                        }
                    }
                    if (!::IntersectRect(&rcTemp, &rcPaint, &pControl->GetPos()))
                        continue;
                    if (pControl->IsFloat())
                    {
                        if (!::IntersectRect(&rcTemp, &m_rcItem, &pControl->GetPos()))
                            continue;
                        CRenderClip::UseOldClipBegin(hDC, childClip);
                        if (!pControl->Paint(hDC, rcPaint, pStopControl))
                            return false;
                        CRenderClip::UseOldClipEnd(hDC, childClip);
                    }
                    else
                    {
                        if (!::IntersectRect(&rcTemp, &rc, &pControl->GetPos()))
                            continue;

                        if (!pControl->Paint(hDC, rcPaint, pStopControl))
                            return false;
                    }

                    auto pSelectRect = m_pOwner->GetMouseSelectRect();
                    if (pSelectRect)
                    {
                        do
                        {
                            if (!::IntersectRect(&rcTemp, &m_rcItem, &pSelectRect->GetPos()))
                                break;
                            CRenderClip::UseOldClipBegin(hDC, childClip);
                            if (!pSelectRect->Paint(hDC, rcPaint, pStopControl))
                                return false;
                            CRenderClip::UseOldClipEnd(hDC, childClip);
                        } while (0);
                    }

                }
            }
        }

        if (m_pVerticalScrollBar != NULL)
        {
            if (m_pVerticalScrollBar == pStopControl) return false;
            if (m_pVerticalScrollBar->IsVisible())
            {
                if (::IntersectRect(&rcTemp, &rcPaint, &m_pVerticalScrollBar->GetPos()))
                {
                    if (!m_pVerticalScrollBar->Paint(hDC, rcPaint, pStopControl)) return false;
                }
            }
        }

        if (m_pHorizontalScrollBar != NULL)
        {
            if (m_pHorizontalScrollBar == pStopControl) return false;
            if (m_pHorizontalScrollBar->IsVisible())
            {
                if (::IntersectRect(&rcTemp, &rcPaint, &m_pHorizontalScrollBar->GetPos()))
                {
                    if (!m_pHorizontalScrollBar->Paint(hDC, rcPaint, pStopControl)) return false;
                }
            }
        }
        return true;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListHeaderUI::CListHeaderUI()
    {}

    LPCTSTR CListHeaderUI::GetClass() const
    {
        return DUI_CTR_LISTHEADER;
    }

    LPVOID CListHeaderUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_LISTHEADER) == 0) return this;
        return CHorizontalLayoutUI::GetInterface(pstrName);
    }

    SIZE CListHeaderUI::EstimateSize(SIZE szAvailable)
    {
        SIZE cXY = { 0, m_cxyFixed.cy };
        if (cXY.cy == 0 && m_pManager != NULL)
        {
            for (int it = 0; it < m_items.GetSize(); it++)
            {
                cXY.cy = MAX(cXY.cy, static_cast<CControlUI*>(m_items[it])->EstimateSize(szAvailable).cy);
            }
            int nMin = m_pManager->GetDefaultFontInfo()->tm.tmHeight + 8;
            cXY.cy = MAX(cXY.cy, nMin);
        }

        for (int it = 0; it < m_items.GetSize(); it++)
        {
            cXY.cx += static_cast<CControlUI*>(m_items[it])->EstimateSize(szAvailable).cx;
        }

        return cXY;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListHeaderItemUI::CListHeaderItemUI() : m_bDragable(true), m_uButtonState(0), m_iSepWidth(4),
        m_uTextStyle(DT_CENTER | DT_VCENTER | DT_SINGLELINE), m_dwTextColor(0), m_dwSepColor(0),
        m_iFont(-1), m_bShowHtml(false)
    {
        SetTextPadding(CDuiRect(2, 0, 2, 0));
        ptLastMouse.x = ptLastMouse.y = 0;
        SetMinWidth(16);
    }

    LPCTSTR CListHeaderItemUI::GetClass() const
    {
        return DUI_CTR_LISTHEADERITEM;
    }

    LPVOID CListHeaderItemUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_LISTHEADERITEM) == 0) return this;
        return CControlUI::GetInterface(pstrName);
    }

    UINT CListHeaderItemUI::GetControlFlags() const
    {
        if (IsEnabled() && m_iSepWidth != 0) return UIFLAG_SETCURSOR;
        else return 0;
    }

    void CListHeaderItemUI::SetEnabled(bool bEnable)
    {
        CControlUI::SetEnabled(bEnable);
        if (!IsEnabled())
        {
            m_uButtonState = 0;
        }
    }

    bool CListHeaderItemUI::IsDragable() const
    {
        return m_bDragable;
    }

    void CListHeaderItemUI::SetDragable(bool bDragable)
    {
        m_bDragable = bDragable;
        if (!m_bDragable) m_uButtonState &= ~UISTATE_CAPTURED;
    }

    int CListHeaderItemUI::GetIndex() const
    {
        return m_Index;
    }

    void CListHeaderItemUI::SetIndex(int new_index)
    {
        m_Index = new_index;
    }

    DWORD CListHeaderItemUI::GetSepWidth() const
    {
        return m_iSepWidth;
    }

    void CListHeaderItemUI::SetSepWidth(int iWidth)
    {
        m_iSepWidth = iWidth;
    }

    DWORD CListHeaderItemUI::GetTextStyle() const
    {
        return m_uTextStyle;
    }

    void CListHeaderItemUI::SetTextStyle(UINT uStyle)
    {
        m_uTextStyle = uStyle;
        Invalidate();
    }

    DWORD CListHeaderItemUI::GetTextColor() const
    {
        return m_dwTextColor;
    }


    void CListHeaderItemUI::SetTextColor(DWORD dwTextColor)
    {
        m_dwTextColor = dwTextColor;
        Invalidate();
    }

    DWORD CListHeaderItemUI::GetSepColor() const
    {
        return m_dwSepColor;
    }

    void CListHeaderItemUI::SetSepColor(DWORD dwSepColor)
    {
        m_dwSepColor = dwSepColor;
        Invalidate();
    }

    RECT CListHeaderItemUI::GetTextPadding() const
    {
        return m_rcTextPadding;
    }

    void CListHeaderItemUI::SetTextPadding(RECT rc)
    {
        m_rcTextPadding = rc;
        Invalidate();
    }

    void CListHeaderItemUI::SetFont(int index)
    {
        m_iFont = index;
    }

    bool CListHeaderItemUI::IsShowHtml()
    {
        return m_bShowHtml;
    }

    void CListHeaderItemUI::SetShowHtml(bool bShowHtml)
    {
        if (m_bShowHtml == bShowHtml) return;

        m_bShowHtml = bShowHtml;
        Invalidate();
    }

    LPCTSTR CListHeaderItemUI::GetNormalImage() const
    {
        return m_diNormal.sDrawString;
    }

    void CListHeaderItemUI::SetNormalImage(LPCTSTR pStrImage)
    {
        if (m_diNormal.sDrawString == pStrImage && m_diNormal.pImageInfo != NULL) return;
        m_diNormal.Clear();
        m_diNormal.sDrawString = pStrImage;
        Invalidate();
    }

    LPCTSTR CListHeaderItemUI::GetHotImage() const
    {
        return m_diHot.sDrawString;
    }

    void CListHeaderItemUI::SetHotImage(LPCTSTR pStrImage)
    {
        if (m_diHot.sDrawString == pStrImage && m_diHot.pImageInfo != NULL) return;
        m_diHot.Clear();
        m_diHot.sDrawString = pStrImage;
        Invalidate();
    }

    LPCTSTR CListHeaderItemUI::GetPushedImage() const
    {
        return m_diPushed.sDrawString;
    }

    void CListHeaderItemUI::SetPushedImage(LPCTSTR pStrImage)
    {
        if (m_diPushed.sDrawString == pStrImage && m_diPushed.pImageInfo != NULL) return;
        m_diPushed.Clear();
        m_diPushed.sDrawString = pStrImage;
        Invalidate();
    }

    LPCTSTR CListHeaderItemUI::GetFocusedImage() const
    {
        return m_diFocused.sDrawString;
    }

    void CListHeaderItemUI::SetFocusedImage(LPCTSTR pStrImage)
    {
        if (m_diFocused.sDrawString == pStrImage && m_diFocused.pImageInfo != NULL) return;
        m_diFocused.Clear();
        m_diFocused.sDrawString = pStrImage;
        Invalidate();
    }

    LPCTSTR CListHeaderItemUI::GetSepImage() const
    {
        return m_diSep.sDrawString;
    }

    void CListHeaderItemUI::SetSepImage(LPCTSTR pStrImage)
    {
        if (m_diSep.sDrawString == pStrImage && m_diSep.pImageInfo != NULL) return;
        m_diSep.Clear();
        m_diSep.sDrawString = pStrImage;
        Invalidate();
    }

    void CListHeaderItemUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
    {
        if (_tcscmp(pstrName, _T("dragable")) == 0) SetDragable(_tcscmp(pstrValue, _T("true")) == 0);
        else if (_tcscmp(pstrName, _T("align")) == 0)
        {
            if (_tcsstr(pstrValue, _T("left")) != NULL)
            {
                m_uTextStyle &= ~(DT_CENTER | DT_RIGHT);
                m_uTextStyle |= DT_LEFT;
            }
            if (_tcsstr(pstrValue, _T("center")) != NULL)
            {
                m_uTextStyle &= ~(DT_LEFT | DT_RIGHT);
                m_uTextStyle |= DT_CENTER;
            }
            if (_tcsstr(pstrValue, _T("right")) != NULL)
            {
                m_uTextStyle &= ~(DT_LEFT | DT_CENTER);
                m_uTextStyle |= DT_RIGHT;
            }
        }
        else if (_tcscmp(pstrName, _T("valign")) == 0)
        {
            if (_tcsstr(pstrValue, _T("top")) != NULL)
            {
                m_uTextStyle &= ~(DT_BOTTOM | DT_VCENTER);
                m_uTextStyle |= DT_TOP;
            }
            if (_tcsstr(pstrValue, _T("vcenter")) != NULL)
            {
                m_uTextStyle &= ~(DT_TOP | DT_BOTTOM);
                m_uTextStyle |= DT_VCENTER;
            }
            if (_tcsstr(pstrValue, _T("bottom")) != NULL)
            {
                m_uTextStyle &= ~(DT_TOP | DT_VCENTER);
                m_uTextStyle |= DT_BOTTOM;
            }
        }
        else if (_tcscmp(pstrName, _T("endellipsis")) == 0)
        {
            if (_tcscmp(pstrValue, _T("true")) == 0) m_uTextStyle |= DT_END_ELLIPSIS;
            else m_uTextStyle &= ~DT_END_ELLIPSIS;
        }
        else if (_tcscmp(pstrName, _T("multiline")) == 0)
        {
            if (_tcscmp(pstrValue, _T("true")) == 0)
            {
                m_uTextStyle &= ~DT_SINGLELINE;
                m_uTextStyle |= DT_WORDBREAK;
            }
            else m_uTextStyle |= DT_SINGLELINE;
        }
        else if (_tcscmp(pstrName, _T("font")) == 0) SetFont(_ttoi(pstrValue));
        else if (_tcscmp(pstrName, _T("textcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetTextColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("textpadding")) == 0)
        {
            RECT rcTextPadding = { 0 };
            LPTSTR pstr = NULL;
            rcTextPadding.left = _tcstol(pstrValue, &pstr, 10);  ASSERT(pstr);
            rcTextPadding.top = _tcstol(pstr + 1, &pstr, 10);    ASSERT(pstr);
            rcTextPadding.right = _tcstol(pstr + 1, &pstr, 10);  ASSERT(pstr);
            rcTextPadding.bottom = _tcstol(pstr + 1, &pstr, 10); ASSERT(pstr);
            SetTextPadding(rcTextPadding);
        }
        else if (_tcscmp(pstrName, _T("showhtml")) == 0) SetShowHtml(_tcscmp(pstrValue, _T("true")) == 0);
        else if (_tcscmp(pstrName, _T("normalimage")) == 0) SetNormalImage(pstrValue);
        else if (_tcscmp(pstrName, _T("hotimage")) == 0) SetHotImage(pstrValue);
        else if (_tcscmp(pstrName, _T("pushedimage")) == 0) SetPushedImage(pstrValue);
        else if (_tcscmp(pstrName, _T("focusedimage")) == 0) SetFocusedImage(pstrValue);
        else if (_tcscmp(pstrName, _T("sepwidth")) == 0) SetSepWidth(_ttoi(pstrValue));
        else if (_tcscmp(pstrName, _T("sepcolor")) == 0)
        {
            if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
            LPTSTR pstr = NULL;
            DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
            SetSepColor(clrColor);
        }
        else if (_tcscmp(pstrName, _T("sepimage")) == 0) SetSepImage(pstrValue);
        else CControlUI::SetAttribute(pstrName, pstrValue);
    }

    void CListHeaderItemUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pParent != NULL) m_pParent->DoEvent(event);
            else CControlUI::DoEvent(event);
            return;
        }

        if (event.Type == UIEVENT_SETFOCUS)
        {
            Invalidate();
        }
        if (event.Type == UIEVENT_KILLFOCUS)
        {
            Invalidate();
        }
        if (event.Type == UIEVENT_BUTTONDOWN || event.Type == UIEVENT_DBLCLICK)
        {
            if (!IsEnabled()) return;
            RECT rcSeparator = GetThumbRect();
            if (::PtInRect(&rcSeparator, event.ptMouse))
            {
                if (m_bDragable)
                {
                    m_uButtonState |= UISTATE_CAPTURED;
                    ptLastMouse = event.ptMouse;
                }
            }
            else
            {
                m_uButtonState |= UISTATE_PUSHED;
                auto compare_type = static_cast<int>(m_CompareType);
                m_pManager->SendNotify(this, DUI_MSGTYPE_HEADERCLICK, (WPARAM)m_Index, (LPARAM)compare_type);
                Invalidate();
            }
            return;
        }
        if (event.Type == UIEVENT_BUTTONUP)
        {
            if ((m_uButtonState & UISTATE_CAPTURED) != 0)
            {
                m_uButtonState &= ~UISTATE_CAPTURED;
                if (GetParent())
                    GetParent()->NeedParentUpdate();
            }
            else if ((m_uButtonState & UISTATE_PUSHED) != 0)
            {
                m_uButtonState &= ~UISTATE_PUSHED;
                Invalidate();
            }
            return;
        }
        if (event.Type == UIEVENT_MOUSEMOVE)
        {
            if ((m_uButtonState & UISTATE_CAPTURED) != 0)
            {
                RECT rc = m_rcItem;
                if (m_iSepWidth >= 0)
                {
                    rc.right -= ptLastMouse.x - event.ptMouse.x;
                }
                else
                {
                    rc.left -= ptLastMouse.x - event.ptMouse.x;
                }

                if (rc.right - rc.left > GetMinWidth())
                {
                    m_cxyFixed.cx = rc.right - rc.left;
                    ptLastMouse = event.ptMouse;
                    if (GetParent())
                        GetParent()->NeedParentUpdate();
                }
            }
            return;
        }
        if (event.Type == UIEVENT_SETCURSOR)
        {
            RECT rcSeparator = GetThumbRect();
            if (IsEnabled() && m_bDragable && ::PtInRect(&rcSeparator, event.ptMouse))
            {
                ::SetCursor(::LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZEWE)));
                return;
            }
        }
        if (event.Type == UIEVENT_MOUSEENTER)
        {
            if (::PtInRect(&m_rcItem, event.ptMouse))
            {
                if (IsEnabled())
                {
                    if ((m_uButtonState & UISTATE_HOT) == 0)
                    {
                        m_uButtonState |= UISTATE_HOT;
                        Invalidate();
                    }
                }
            }
        }
        if (event.Type == UIEVENT_MOUSELEAVE)
        {
            if (!::PtInRect(&m_rcItem, event.ptMouse))
            {
                if (IsEnabled())
                {
                    if ((m_uButtonState & UISTATE_HOT) != 0)
                    {
                        m_uButtonState &= ~UISTATE_HOT;
                        Invalidate();
                    }
                }
                if (m_pManager) m_pManager->RemoveMouseLeaveNeeded(this);
            }
            else
            {
                if (m_pManager) m_pManager->AddMouseLeaveNeeded(this);
                return;
            }
        }
        CControlUI::DoEvent(event);
    }

    SIZE CListHeaderItemUI::EstimateSize(SIZE szAvailable)
    {
        SIZE result = { 0 };
        do
        {
            if (m_cxyFixed.cy == 0)
            {
                CDuiSize _temp(m_cxyFixed.cx, m_pManager->GetDefaultFontInfo()->tm.tmHeight + 8);
                result = dynamic_cast<SIZE&>(_temp);
            }
            else
            {
                result = CControlUI::EstimateSize(szAvailable);
            }

        } while (0);

        return result;
        //if (m_cxyFixed.cy == 0) return CDuiSize(m_cxyFixed.cx, m_pManager->GetDefaultFontInfo()->tm.tmHeight + 8);
        //return CControlUI::EstimateSize(szAvailable);
    }

    RECT CListHeaderItemUI::GetThumbRect() const
    {
        if (m_iSepWidth >= 0)
            return RECT{ m_rcItem.right - m_iSepWidth, m_rcItem.top, m_rcItem.right, m_rcItem.bottom };
        else
            return RECT{ m_rcItem.left, m_rcItem.top, m_rcItem.left - m_iSepWidth, m_rcItem.bottom };
    }

    void CListHeaderItemUI::PaintStatusImage(HDC hDC)
    {
        if (IsFocused()) m_uButtonState |= UISTATE_FOCUSED;
        else m_uButtonState &= ~UISTATE_FOCUSED;

        if ((m_uButtonState & UISTATE_PUSHED) != 0)
        {
            if (!DrawImage(hDC, m_diPushed))  DrawImage(hDC, m_diNormal);
        }
        else if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            if (!DrawImage(hDC, m_diHot))  DrawImage(hDC, m_diNormal);
        }
        else if ((m_uButtonState & UISTATE_FOCUSED) != 0)
        {
            if (!DrawImage(hDC, m_diFocused))  DrawImage(hDC, m_diNormal);
        }
        else
        {
            DrawImage(hDC, m_diNormal);
        }

        if (m_iSepWidth > 0)
        {
            RECT rcThumb = GetThumbRect();
            m_diSep.rcDestOffset.left = rcThumb.left - m_rcItem.left;
            m_diSep.rcDestOffset.top = rcThumb.top - m_rcItem.top;
            m_diSep.rcDestOffset.right = rcThumb.right - m_rcItem.left;
            m_diSep.rcDestOffset.bottom = rcThumb.bottom - m_rcItem.top;
            if (!DrawImage(hDC, m_diSep))
            {
                if (m_dwSepColor != 0)
                {
                    RECT rcSepLine = { rcThumb.left + m_iSepWidth / 2, rcThumb.top, rcThumb.left + m_iSepWidth / 2, rcThumb.bottom };
                    CRenderEngine::DrawLine(hDC, rcSepLine, m_iSepWidth, GetAdjustColor(m_dwSepColor));
                }
            }
        }
    }

    void CListHeaderItemUI::SetCompareType(CompareType new_compare_type)
    {
        m_CompareType = new_compare_type;
    }

    DuiLib::CListHeaderItemUI::CompareType CListHeaderItemUI::GetCompareType() const
    {
        return m_CompareType;
    }

    void CListHeaderItemUI::PaintText(HDC hDC)
    {
        if (m_dwTextColor == 0) m_dwTextColor = m_pManager->GetDefaultFontColor();

        RECT rcText = m_rcItem;
        rcText.left += m_rcTextPadding.left;
        rcText.top += m_rcTextPadding.top;
        rcText.right -= m_rcTextPadding.right;
        rcText.bottom -= m_rcTextPadding.bottom;

        if (m_sText.IsEmpty()) return;
        int nLinks = 0;
        if (m_bShowHtml)
            CRenderEngine::DrawHtmlText(hDC, m_pManager, rcText, m_sText, m_dwTextColor, \
                                        NULL, NULL, nLinks, m_iFont, m_uTextStyle);
        else
            CRenderEngine::DrawText(hDC, m_pManager, rcText, m_sText, m_dwTextColor, \
                                    m_iFont, m_uTextStyle);
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListElementUI::CListElementUI() :
        m_iIndex(-1),
        m_iDrawIndex(0),
        m_pOwner(NULL),
        m_bSelected(false),
        m_uButtonState(0)
    {}

    LPCTSTR CListElementUI::GetClass() const
    {
        return DUI_CTR_LISTELEMENT;
    }

    UINT CListElementUI::GetControlFlags() const
    {
        return UIFLAG_WANTRETURN;
    }

    LPVOID CListElementUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_ILISTITEM) == 0) return static_cast<IListItemUI*>(this);
        if (_tcscmp(pstrName, DUI_CTR_LISTELEMENT) == 0) return static_cast<CListElementUI*>(this);
        return CControlUI::GetInterface(pstrName);
    }

    IListOwnerUI* CListElementUI::GetOwner()
    {
        return m_pOwner;
    }

    void CListElementUI::SetOwner(CControlUI* pOwner)
    {
        if (pOwner != NULL) m_pOwner = static_cast<IListOwnerUI*>(pOwner->GetInterface(DUI_CTR_ILISTOWNER));
    }

    void CListElementUI::SetVisible(bool bVisible)
    {
        CControlUI::SetVisible(bVisible);
        if (!IsVisible() && m_bSelected)
        {
            m_bSelected = false;
            if (m_pOwner != NULL) m_pOwner->SelectItem(-1);
        }
    }

    void CListElementUI::SetEnabled(bool bEnable)
    {
        CControlUI::SetEnabled(bEnable);
        if (!IsEnabled())
        {
            m_uButtonState = 0;
        }
    }

    int CListElementUI::GetIndex() const
    {
        return m_iIndex;
    }

    void CListElementUI::SetIndex(int iIndex)
    {
        m_iIndex = iIndex;
    }

    int CListElementUI::GetDrawIndex() const
    {
        return m_iDrawIndex;
    }

    void CListElementUI::SetDrawIndex(int iIndex)
    {
        m_iDrawIndex = iIndex;
    }

    void CListElementUI::Invalidate()
    {
        if (!IsVisible()) return;

        if (GetParent())
        {
            CContainerUI* pParentContainer = static_cast<CContainerUI*>(GetParent()->GetInterface(DUI_CTR_CONTAINER));
            if (pParentContainer)
            {
                RECT rc = pParentContainer->GetPos();
                RECT rcInset = pParentContainer->GetInset();
                rc.left += rcInset.left;
                rc.top += rcInset.top;
                rc.right -= rcInset.right;
                rc.bottom -= rcInset.bottom;
                CScrollBarUI* pVerticalScrollBar = pParentContainer->GetVerticalScrollBar();
                if (pVerticalScrollBar && pVerticalScrollBar->IsVisible()) rc.right -= pVerticalScrollBar->GetFixedWidth();
                CScrollBarUI* pHorizontalScrollBar = pParentContainer->GetHorizontalScrollBar();
                if (pHorizontalScrollBar && pHorizontalScrollBar->IsVisible()) rc.bottom -= pHorizontalScrollBar->GetFixedHeight();

                RECT invalidateRc = m_rcItem;
                if (!::IntersectRect(&invalidateRc, &m_rcItem, &rc))
                {
                    return;
                }

                CControlUI* pParent = GetParent();
                RECT rcTemp;
                RECT rcParent;
                while (pParent = pParent->GetParent())
                {
                    rcTemp = invalidateRc;
                    rcParent = pParent->GetPos();
                    if (!::IntersectRect(&invalidateRc, &rcTemp, &rcParent))
                    {
                        return;
                    }
                }

                if (m_pManager != NULL) m_pManager->Invalidate(invalidateRc);
            }
            else
            {
                CControlUI::Invalidate();
            }
        }
        else
        {
            CControlUI::Invalidate();
        }
    }

    bool CListElementUI::Activate()
    {
        if (!CControlUI::Activate()) return false;
        if (m_pManager != NULL) m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMACTIVATE);
        return true;
    }

    bool CListElementUI::IsSelected() const
    {
        return m_bSelected;
    }

    bool CListElementUI::Select(bool bSelect, bool bCallback, bool bTriggerEvent)
    {
        bool result = false;
        do
        {
            if (IsEnabled() == false)
                break;
            // ClistUI、 CComboUI
            bool bShift = (GetKeyState(VK_SHIFT) & 0x8000);
            bool bCtrl = (GetKeyState(VK_CONTROL) & 0x8000);
            if (bCtrl == true)
            {
                if (m_pManager->GetFocus() == this)
                {
                    m_bSelected = !m_bSelected;
                }
            }
            else
            {
                m_bSelected = bSelect;
            }

            Hot(m_bSelected);

            if (bCallback && m_pOwner != NULL)
            {
                bShift == true ? m_pOwner->SelectRange(m_iIndex) : m_pOwner->SelectItem(m_iIndex);
            }

            Invalidate();
            result = true;
        } while (0);

        return result;
    }

    bool CListElementUI::IsExpanded() const
    {
        return false;
    }

    bool CListElementUI::Expand(bool /*bExpand = true*/)
    {
        return false;
    }

    void CListElementUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pOwner != NULL) m_pOwner->DoEvent(event);
            else CControlUI::DoEvent(event);
            return;
        }

        if (event.Type == UIEVENT_DBLCLICK)
        {
            if (IsEnabled())
            {
                Activate();
                Invalidate();
            }

            if (m_pOwner != NULL)
                m_pOwner->DoEvent(event);
            return;
        }
        if (event.Type == UIEVENT_KEYDOWN)
        {
            if (IsKeyboardEnabled() && IsEnabled())
            {
                if (event.chKey == VK_RETURN)
                {
                    Activate();
                    Invalidate();
                    return;
                }
            }
        }
        // An important twist: The list-item will send the event not to its immediate
        // parent but to the "attached" list. A list may actually embed several components
        // in its path to the item, but key-presses etc. needs to go to the actual list.
        if (m_pOwner != NULL)
            m_pOwner->DoEvent(event);
        else
            CControlUI::DoEvent(event);
    }

    void CListElementUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
    {
        if (_tcscmp(pstrName, _T("selected")) == 0) Select();
        else CControlUI::SetAttribute(pstrName, pstrValue);
    }

    void CListElementUI::DrawItemBk(HDC hDC, const RECT& rcItem)
    {
        ASSERT(m_pOwner);
        if (m_pOwner == NULL) return;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return;
        DWORD iBackColor = 0;
        if (!pInfo->bAlternateBk || m_iDrawIndex % 2 == 0) iBackColor = pInfo->dwBkColor;
        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            iBackColor = pInfo->dwHotBkColor;
        }
        if (IsSelected())
        {
            iBackColor = pInfo->dwSelectedBkColor;
        }
        if (!IsEnabled())
        {
            iBackColor = pInfo->dwDisabledBkColor;
        }

        if (iBackColor != 0)
        {
            CRenderEngine::DrawColor(hDC, rcItem, GetAdjustColor(iBackColor));
        }

        if (!IsEnabled())
        {
            if (DrawImage(hDC, pInfo->diDisabled)) return;
        }
        if (IsSelected())
        {
            if (DrawImage(hDC, pInfo->diSelected)) return;
        }
        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            if (DrawImage(hDC, pInfo->diHot)) return;
        }

        if (!DrawImage(hDC, m_diBk))
        {
            if (!pInfo->bAlternateBk || m_iDrawIndex % 2 == 0)
            {
                if (DrawImage(hDC, pInfo->diBk)) return;
            }
        }
    }

    bool CListElementUI::Hot(bool bHot)
    {
        if (!IsEnabled()) return false;
        auto oo1 = dynamic_cast<CListUI*>(m_pOwner);

        BOOL bCtrl = (GetKeyState(VK_CONTROL) & 0x8000);
        //const char* ooiu = oo1->GetName().GetData();
        //auto wewe = strcmp(ooiu, "PartitionTreeCtrl");
        //if (bCtrl && wewe ==0)
        //{
        //    debughelper::DebugPrint(0, "%s 调用DuiLib::CListUI::DoEvent(event)\n", oo1->GetName().GetData());
        //}

        if (bHot)
        {
            m_uButtonState |= UISTATE_HOT;
        }
        else
        {
            if ((m_uButtonState & UISTATE_HOT) != 0)
            {
                m_uButtonState &= ~UISTATE_HOT;
            }
        }
        //m_pOwner->HotItem(m_iIndex, bHot);
        Invalidate();

        return true;
    }

    bool CListElementUI::IsHot()
    {
        return m_uButtonState & UISTATE_HOT;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListLabelElementUI::CListLabelElementUI() : m_bNeedEstimateSize(true), m_uFixedHeightLast(0),
        m_nFontLast(-1), m_uTextStyleLast(0)
    {
        m_szAvailableLast.cx = m_szAvailableLast.cy = 0;
        m_cxyFixedLast.cx = m_cxyFixedLast.cy = 0;
        ::ZeroMemory(&m_rcTextPaddingLast, sizeof(m_rcTextPaddingLast));
    }

    LPCTSTR CListLabelElementUI::GetClass() const
    {
        return DUI_CTR_LISTLABELELEMENT;
    }

    LPVOID CListLabelElementUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_LISTLABELELEMENT) == 0)
            return static_cast<CListLabelElementUI*>(this);
        return CListElementUI::GetInterface(pstrName);
    }

    void CListLabelElementUI::SetOwner(CControlUI* pOwner)
    {
        m_bNeedEstimateSize = true;
        CListElementUI::SetOwner(pOwner);
    }

    void CListLabelElementUI::SetFixedWidth(int cx)
    {
        m_bNeedEstimateSize = true;
        CControlUI::SetFixedWidth(cx);
    }

    void CListLabelElementUI::SetFixedHeight(int cy)
    {
        m_bNeedEstimateSize = true;
        CControlUI::SetFixedHeight(cy);
    }

    void CListLabelElementUI::SetText(LPCTSTR pstrText)
    {
        m_bNeedEstimateSize = true;
        CControlUI::SetText(pstrText);
    }

    void CListLabelElementUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pOwner != NULL) m_pOwner->DoEvent(event);
            else CListElementUI::DoEvent(event);
            return;
        }

        if (event.Type == UIEVENT_BUTTONDOWN)
        {
            if (IsEnabled() == true)
            {
                m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMCLICK);
                Select();
                Invalidate();
                //debughelper::DebugPrint(0, _T("%s is hot %d \n"), GetName().GetData(), IsHot());
            }
            return;
        }

        if (event.Type == UIEVENT_RBUTTONDOWN)
        {
            if (IsEnabled() == true)
            {
                auto pList = dynamic_cast<CListUI*>(this->GetParent()->GetParent());
                if (pList != nullptr && pList->GetSelectedCount() > 1)
                    return;

                m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMCLICK);
                Select();
                Invalidate();
            }
            return;
        }

        if (event.Type == UIEVENT_MOUSEMOVE)
        {
            return;
        }
        if (event.Type == UIEVENT_BUTTONUP)
        {
            return;
        }
        if (event.Type == UIEVENT_MOUSEENTER)
        {
            if (::PtInRect(&m_rcItem, event.ptMouse))
            {
                if (IsEnabled())
                {
                    if ((m_uButtonState & UISTATE_HOT) == 0)
                    {
                        m_uButtonState |= UISTATE_HOT;
                        Invalidate();
                    }
                }
            }
        }
        if (event.Type == UIEVENT_MOUSELEAVE)
        {
            if (!::PtInRect(&m_rcItem, event.ptMouse))
            {
                if (IsEnabled())
                {
                    if ((m_uButtonState & UISTATE_HOT) != 0)
                    {
                        m_uButtonState &= ~UISTATE_HOT;
                        Invalidate();
                    }
                }
                if (m_pManager) m_pManager->RemoveMouseLeaveNeeded(this);
            }
            else
            {
                if (m_pManager) m_pManager->AddMouseLeaveNeeded(this);
                return;
            }
        }
        CListElementUI::DoEvent(event);
    }

    SIZE CListLabelElementUI::EstimateSize(SIZE szAvailable)
    {
        if (m_pOwner == NULL) return SIZE{ 0, 0 };
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return SIZE{ 0, 0 };
        if (m_cxyFixed.cx > 0)
        {
            if (m_cxyFixed.cy > 0)
                return m_cxyFixed;
            else if (pInfo->uFixedHeight > 0)
            {
                LONG new_fixed_height = pInfo->uFixedHeight;
                return SIZE{ m_cxyFixed.cx, new_fixed_height };
            }
        }

        if ((pInfo->uTextStyle & DT_SINGLELINE) == 0 &&
            (szAvailable.cx != m_szAvailableLast.cx || szAvailable.cy != m_szAvailableLast.cy))
        {
            m_bNeedEstimateSize = true;
        }
        if (m_uFixedHeightLast != pInfo->uFixedHeight || m_nFontLast != pInfo->nFont ||
            m_uTextStyleLast != pInfo->uTextStyle ||
            m_rcTextPaddingLast.left != pInfo->rcTextPadding.left || m_rcTextPaddingLast.right != pInfo->rcTextPadding.right ||
            m_rcTextPaddingLast.top != pInfo->rcTextPadding.top || m_rcTextPaddingLast.bottom != pInfo->rcTextPadding.bottom)
        {
            m_bNeedEstimateSize = true;
        }

        if (m_bNeedEstimateSize)
        {
            m_bNeedEstimateSize = false;
            m_szAvailableLast = szAvailable;
            m_uFixedHeightLast = pInfo->uFixedHeight;
            m_nFontLast = pInfo->nFont;
            m_uTextStyleLast = pInfo->uTextStyle;
            m_rcTextPaddingLast = pInfo->rcTextPadding;

            m_cxyFixedLast = m_cxyFixed;
            if (m_cxyFixedLast.cy == 0)
            {
                m_cxyFixedLast.cy = pInfo->uFixedHeight;
            }

            if ((pInfo->uTextStyle & DT_SINGLELINE) != 0)
            {
                if (m_cxyFixedLast.cy == 0)
                {
                    m_cxyFixedLast.cy = m_pManager->GetFontInfo(pInfo->nFont)->tm.tmHeight + 8;
                    m_cxyFixedLast.cy += pInfo->rcTextPadding.top + pInfo->rcTextPadding.bottom;
                }
                if (m_cxyFixedLast.cx == 0)
                {
                    RECT rcText = { 0, 0, 9999, m_cxyFixedLast.cy };
                    if (pInfo->bShowHtml)
                    {
                        int nLinks = 0;
                        CRenderEngine::DrawHtmlText(m_pManager->GetPaintDC(), m_pManager, rcText, m_sText, 0, NULL, NULL, nLinks, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                    }
                    else
                    {
                        CRenderEngine::DrawText(m_pManager->GetPaintDC(), m_pManager, rcText, m_sText, 0, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                    }
                    m_cxyFixedLast.cx = rcText.right - rcText.left + pInfo->rcTextPadding.left + pInfo->rcTextPadding.right;
                }
            }
            else
            {
                if (m_cxyFixedLast.cx == 0)
                {
                    m_cxyFixedLast.cx = szAvailable.cx;
                }
                RECT rcText = { 0, 0, m_cxyFixedLast.cx, 9999 };
                rcText.left += pInfo->rcTextPadding.left;
                rcText.right -= pInfo->rcTextPadding.right;
                if (pInfo->bShowHtml)
                {
                    int nLinks = 0;
                    CRenderEngine::DrawHtmlText(m_pManager->GetPaintDC(), m_pManager, rcText, m_sText, 0, NULL, NULL, nLinks, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                }
                else
                {
                    CRenderEngine::DrawText(m_pManager->GetPaintDC(), m_pManager, rcText, m_sText, 0, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                }
                m_cxyFixedLast.cy = rcText.bottom - rcText.top + pInfo->rcTextPadding.top + pInfo->rcTextPadding.bottom;
            }
        }
        return m_cxyFixedLast;
    }

    bool CListLabelElementUI::DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl)
    {
        DrawItemBk(hDC, m_rcItem);
        DrawItemText(hDC, m_rcItem);
        return true;
    }

    void CListLabelElementUI::DrawItemText(HDC hDC, const RECT& rcItem)
    {
        if (m_sText.IsEmpty()) return;

        if (m_pOwner == NULL) return;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return;
        DWORD iTextColor = pInfo->dwTextColor;
        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            iTextColor = pInfo->dwHotTextColor;
        }
        if (IsSelected())
        {
            iTextColor = pInfo->dwSelectedTextColor;
        }
        if (!IsEnabled())
        {
            iTextColor = pInfo->dwDisabledTextColor;
        }
        int nLinks = 0;
        RECT rcText = rcItem;
        rcText.left += pInfo->rcTextPadding.left;
        rcText.right -= pInfo->rcTextPadding.right;
        rcText.top += pInfo->rcTextPadding.top;
        rcText.bottom -= pInfo->rcTextPadding.bottom;

        if (pInfo->bShowHtml)
            CRenderEngine::DrawHtmlText(hDC, m_pManager, rcText, m_sText, iTextColor, \
                                        NULL, NULL, nLinks, pInfo->nFont, pInfo->uTextStyle);
        else
            CRenderEngine::DrawText(hDC, m_pManager, rcText, m_sText, iTextColor, \
                                    pInfo->nFont, pInfo->uTextStyle);
    }


    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListTextElementUI::CListTextElementUI() : m_nLinks(0), m_nHoverLink(-1), m_pOwner(NULL)
    {
        ::ZeroMemory(&m_rcLinks, sizeof(m_rcLinks));
    }

    CListTextElementUI::~CListTextElementUI()
    {
        CDuiString* pText;
        for (int it = 0; it < m_aTexts.GetSize(); it++)
        {
            pText = static_cast<CDuiString*>(m_aTexts[it]);
            if (pText)
                delete pText;
        }
        m_pOwner = nullptr;
        m_iIndex = -1;

        m_aTexts.Empty();
    }

    LPCTSTR CListTextElementUI::GetClass() const
    {
        return DUI_CTR_LISTTEXTELEMENT;
    }

    LPVOID CListTextElementUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_LISTTEXTELEMENT) == 0) return static_cast<CListTextElementUI*>(this);
        return CListLabelElementUI::GetInterface(pstrName);
    }

    UINT CListTextElementUI::GetControlFlags() const
    {
        return UIFLAG_WANTRETURN | ((IsEnabled() && m_nLinks > 0) ? UIFLAG_SETCURSOR : 0);
    }

    LPCTSTR CListTextElementUI::GetText(int iIndex)
    {
        CDuiString* pText = static_cast<CDuiString*>(m_aTexts.GetAt(iIndex));
        return pText ? pText->GetData() : NULL;
    }

    void CListTextElementUI::SetText(int iIndex, LPCTSTR pstrText)
    {
        if (m_pOwner == NULL)
            return;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (iIndex < 0 || iIndex >= pInfo->nColumns)
            return;
        m_bNeedEstimateSize = true;

        while (m_aTexts.GetSize() < pInfo->nColumns)
        {
            m_aTexts.Add(NULL);
        }

        CDuiString* pText = static_cast<CDuiString*>(m_aTexts[iIndex]);
        if ((pText == NULL && pstrText == NULL) || (pText && *pText == pstrText)) return;

        if (pText) //by cddjr 2011/10/20
            pText->Assign(pstrText);
        else
            m_aTexts.SetAt(iIndex, new CDuiString(pstrText));
        Invalidate();
    }

    void CListTextElementUI::SetOwner(CControlUI* pOwner)
    {
        if (pOwner != NULL)
        {
            m_bNeedEstimateSize = true;
            CListElementUI::SetOwner(pOwner);
            m_pOwner = static_cast<IListUI*>(pOwner->GetInterface(DUI_CTR_ILIST));
        }
    }

    CDuiString* CListTextElementUI::GetLinkContent(int iIndex)
    {
        if (iIndex >= 0 && iIndex < m_nLinks) return &m_sLinks[iIndex];
        return NULL;
    }

    void CListTextElementUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pOwner != NULL) m_pOwner->DoEvent(event);
            else CListLabelElementUI::DoEvent(event);
            return;
        }

        // When you hover over a link
        if (event.Type == UIEVENT_SETCURSOR)
        {
            for (int i = 0; i < m_nLinks; i++)
            {
                if (::PtInRect(&m_rcLinks[i], event.ptMouse))
                {
                    ::SetCursor(::LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND)));
                    return;
                }
            }
        }
        if (event.Type == UIEVENT_BUTTONUP && IsEnabled())
        {
            for (int i = 0; i < m_nLinks; i++)
            {
                if (::PtInRect(&m_rcLinks[i], event.ptMouse))
                {
                    m_pManager->SendNotify(this, DUI_MSGTYPE_LINK, i);
                    return;
                }
            }
        }
        if (m_nLinks > 0 && event.Type == UIEVENT_MOUSEMOVE)
        {
            int nHoverLink = -1;
            for (int i = 0; i < m_nLinks; i++)
            {
                if (::PtInRect(&m_rcLinks[i], event.ptMouse))
                {
                    nHoverLink = i;
                    break;
                }
            }

            if (m_nHoverLink != nHoverLink)
            {
                Invalidate();
                m_nHoverLink = nHoverLink;
            }
        }
        if (m_nLinks > 0 && event.Type == UIEVENT_MOUSELEAVE)
        {
            if (m_nHoverLink != -1)
            {
                if (!::PtInRect(&m_rcLinks[m_nHoverLink], event.ptMouse))
                {
                    m_nHoverLink = -1;
                    Invalidate();
                    if (m_pManager) m_pManager->RemoveMouseLeaveNeeded(this);
                }
                else
                {
                    if (m_pManager) m_pManager->AddMouseLeaveNeeded(this);
                    return;
                }
            }
        }
        CListLabelElementUI::DoEvent(event);
    }

    SIZE CListTextElementUI::EstimateSize(SIZE szAvailable)
    {
        if (m_pOwner == NULL) return SIZE{ 0,0 };
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return SIZE{ 0,0 };
        SIZE cxyFixed = m_cxyFixed;
        if (cxyFixed.cx == 0 && pInfo->nColumns > 0)
        {
            cxyFixed.cx = pInfo->rcColumn[pInfo->nColumns - 1].right - pInfo->rcColumn[0].left;
            if (m_cxyFixedLast.cx != cxyFixed.cx) m_bNeedEstimateSize = true;
        }
        if (cxyFixed.cx > 0)
        {
            if (cxyFixed.cy > 0) return cxyFixed;
            else if (pInfo->uFixedHeight > 0)
            {
                LONG new_fixed_height = pInfo->uFixedHeight;
                return SIZE{ m_cxyFixed.cx, new_fixed_height };
            }
        }

        if ((pInfo->uTextStyle & DT_SINGLELINE) == 0 &&
            (szAvailable.cx != m_szAvailableLast.cx || szAvailable.cy != m_szAvailableLast.cy))
        {
            m_bNeedEstimateSize = true;
        }
        if (m_uFixedHeightLast != pInfo->uFixedHeight || m_nFontLast != pInfo->nFont ||
            m_uTextStyleLast != pInfo->uTextStyle ||
            m_rcTextPaddingLast.left != pInfo->rcTextPadding.left || m_rcTextPaddingLast.right != pInfo->rcTextPadding.right ||
            m_rcTextPaddingLast.top != pInfo->rcTextPadding.top || m_rcTextPaddingLast.bottom != pInfo->rcTextPadding.bottom)
        {
            m_bNeedEstimateSize = true;
        }

        CDuiString strText;
        IListCallbackUI* pCallback = m_pOwner->GetTextCallback();
        if (pCallback) 
            strText = pCallback->GetItemText(this, m_iIndex, 0);
        else if (m_aTexts.GetSize() > 0) 
            strText.Assign(GetText(0));
        else 
            strText = m_sText;

        if (m_sTextLast != strText) m_bNeedEstimateSize = true;

        if (m_bNeedEstimateSize)
        {
            m_bNeedEstimateSize = false;
            m_szAvailableLast = szAvailable;
            m_uFixedHeightLast = pInfo->uFixedHeight;
            m_nFontLast = pInfo->nFont;
            m_uTextStyleLast = pInfo->uTextStyle;
            m_rcTextPaddingLast = pInfo->rcTextPadding;
            m_sTextLast = strText;

            m_cxyFixedLast = m_cxyFixed;
            if (m_cxyFixedLast.cx == 0 && pInfo->nColumns > 0)
            {
                m_cxyFixedLast.cx = pInfo->rcColumn[pInfo->nColumns - 1].right - pInfo->rcColumn[0].left;
            }
            if (m_cxyFixedLast.cy == 0)
            {
                m_cxyFixedLast.cy = pInfo->uFixedHeight;
            }

            if ((pInfo->uTextStyle & DT_SINGLELINE) != 0)
            {
                if (m_cxyFixedLast.cy == 0)
                {
                    m_cxyFixedLast.cy = m_pManager->GetFontInfo(pInfo->nFont)->tm.tmHeight + 8;
                    m_cxyFixedLast.cy += pInfo->rcTextPadding.top + pInfo->rcTextPadding.bottom;
                }
                if (m_cxyFixedLast.cx == 0)
                {
                    RECT rcText = { 0, 0, 9999, m_cxyFixedLast.cy };
                    if (pInfo->bShowHtml)
                    {
                        int nLinks = 0;
                        CRenderEngine::DrawHtmlText(m_pManager->GetPaintDC(), m_pManager, rcText, strText, 0, NULL, NULL, nLinks, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                    }
                    else
                    {
                        CRenderEngine::DrawText(m_pManager->GetPaintDC(), m_pManager, rcText, strText, 0, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                    }
                    m_cxyFixedLast.cx = rcText.right - rcText.left + pInfo->rcTextPadding.left + pInfo->rcTextPadding.right;
                }
            }
            else
            {
                if (m_cxyFixedLast.cx == 0)
                {
                    m_cxyFixedLast.cx = szAvailable.cx;
                }
                RECT rcText = { 0, 0, m_cxyFixedLast.cx, 9999 };
                rcText.left += pInfo->rcTextPadding.left;
                rcText.right -= pInfo->rcTextPadding.right;
                if (pInfo->bShowHtml)
                {
                    int nLinks = 0;
                    CRenderEngine::DrawHtmlText(m_pManager->GetPaintDC(), m_pManager, rcText, strText, 0, NULL, NULL, nLinks, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                }
                else
                {
                    CRenderEngine::DrawText(m_pManager->GetPaintDC(), m_pManager, rcText, strText, 0, pInfo->nFont, DT_CALCRECT | pInfo->uTextStyle & ~DT_RIGHT & ~DT_CENTER);
                }
                m_cxyFixedLast.cy = rcText.bottom - rcText.top + pInfo->rcTextPadding.top + pInfo->rcTextPadding.bottom;
            }
        }
        return m_cxyFixedLast;
    }

    void CListTextElementUI::DrawItemText(HDC hDC, const RECT& rcItem)
    {
        if (m_pOwner == NULL) return;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return;
        DWORD iTextColor = pInfo->dwTextColor;

        m_hDC = hDC;
        m_RercItem = rcItem;

        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            iTextColor = pInfo->dwHotTextColor;
        }
        if (IsSelected())
        {
            iTextColor = pInfo->dwSelectedTextColor;
        }
        if (!IsEnabled())
        {
            iTextColor = pInfo->dwDisabledTextColor;
        }
        IListCallbackUI* pCallback = m_pOwner->GetTextCallback();

        m_nLinks = 0;
        int nLinks = lengthof(m_rcLinks);
        if (pInfo->nColumns > 0)
        {
            for (int i = 0; i < pInfo->nColumns; i++)
            {
                RECT rcItem = { pInfo->rcColumn[i].left, m_rcItem.top, pInfo->rcColumn[i].right, m_rcItem.bottom };
                if (pInfo->iVLineSize > 0 && i < pInfo->nColumns - 1)
                {
                    RECT rcLine = { rcItem.right - pInfo->iVLineSize / 2, rcItem.top, rcItem.right - pInfo->iVLineSize / 2, rcItem.bottom };
                    CRenderEngine::DrawLine(hDC, rcLine, pInfo->iVLineSize, GetAdjustColor(pInfo->dwVLineColor));
                    rcItem.right -= pInfo->iVLineSize;
                }

                rcItem.left += pInfo->rcTextPadding.left;
                rcItem.right -= pInfo->rcTextPadding.right;
                rcItem.top += pInfo->rcTextPadding.top;
                rcItem.bottom -= pInfo->rcTextPadding.bottom;

                CDuiString item_text;
                if (GetText(i) != nullptr && pCallback != nullptr)
                {
                    item_text = pCallback->GetItemText(this, m_iIndex, i);
                    //SetText(i, item_text.c_str());
                }
                else if (GetText(i) != nullptr)
                {
                    item_text = GetText(i);
                }
                else if (pCallback != nullptr)
                {
                    item_text = pCallback->GetItemText(this, m_iIndex, i);
                    SetText(i, item_text);
                }
                if (pInfo->bShowHtml)
                {
                    CRenderEngine::DrawHtmlText(hDC, m_pManager, rcItem, item_text, iTextColor, &m_rcLinks[m_nLinks], &m_sLinks[m_nLinks], nLinks, pInfo->nFont, pInfo->uTextStyle);
                }
                else
                {
                    CRenderEngine::DrawText(hDC, m_pManager, rcItem, item_text, iTextColor, pInfo->nFont, pInfo->uTextStyle);
                }

                //CDuiString strText;//不使用LPCTSTR，否则限制太多 by cddjr 2011/10/20
                //if (pCallback) strText = pCallback->GetItemText(this, m_iIndex, i);
                //else strText.Assign(GetText(i));
                //if (pInfo->bShowHtml)
                //    CRenderEngine::DrawHtmlText(hDC, m_pManager, rcItem, strText.GetData(), iTextColor, &m_rcLinks[m_nLinks], &m_sLinks[m_nLinks], nLinks, pInfo->nFont, pInfo->uTextStyle);
                //else
                //    CRenderEngine::DrawText(hDC, m_pManager, rcItem, strText.GetData(), iTextColor, pInfo->nFont, pInfo->uTextStyle);

                m_nLinks += nLinks;
                nLinks = lengthof(m_rcLinks) - m_nLinks;
            }
        }
        else
        {
            RECT rcItem = m_rcItem;
            rcItem.left += pInfo->rcTextPadding.left;
            rcItem.right -= pInfo->rcTextPadding.right;
            rcItem.top += pInfo->rcTextPadding.top;
            rcItem.bottom -= pInfo->rcTextPadding.bottom;

            CDuiString strText;
            if (pCallback) strText = pCallback->GetItemText(this, m_iIndex, 0);
            else if (m_aTexts.GetSize() > 0) strText.Assign(GetText(0));
            else strText = m_sText;
            if (pInfo->bShowHtml)
                CRenderEngine::DrawHtmlText(hDC, m_pManager, rcItem, strText.GetData(), iTextColor, \
                                            & m_rcLinks[m_nLinks], &m_sLinks[m_nLinks], nLinks, pInfo->nFont, pInfo->uTextStyle);
            else
                CRenderEngine::DrawText(hDC, m_pManager, rcItem, strText.GetData(), iTextColor, \
                                        pInfo->nFont, pInfo->uTextStyle);

            m_nLinks += nLinks;
            nLinks = lengthof(m_rcLinks) - m_nLinks;
        }

        for (int i = m_nLinks; i < lengthof(m_rcLinks); i++)
        {
            ::ZeroMemory(m_rcLinks + i, sizeof(RECT));
            ((CDuiString*)(m_sLinks + i))->Empty();
        }
    }

    void CListTextElementUI::ReDrawItemText()
    {
        if (m_pOwner == NULL) return;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return;
        DWORD iTextColor = pInfo->dwTextColor;

        HDC II1 = this->GetManager()->GetPaintDC();
        HDC hDC = m_hDC;
        const RECT& rcItem = m_RercItem;

        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            iTextColor = pInfo->dwHotTextColor;
        }
        if (IsSelected())
        {
            iTextColor = pInfo->dwSelectedTextColor;
        }
        if (!IsEnabled())
        {
            iTextColor = pInfo->dwDisabledTextColor;
        }
        IListCallbackUI* pCallback = m_pOwner->GetTextCallback();

        m_nLinks = 0;
        int nLinks = lengthof(m_rcLinks);
        if (pInfo->nColumns <= 0)
            return;

        for (int i = 0; i < pInfo->nColumns; i++)
        {
            RECT rcItem = { pInfo->rcColumn[i].left, m_rcItem.top, pInfo->rcColumn[i].right, m_rcItem.bottom };
            if (pInfo->iVLineSize > 0 && i < pInfo->nColumns - 1)
            {
                RECT rcLine = { rcItem.right - pInfo->iVLineSize / 2, rcItem.top, rcItem.right - pInfo->iVLineSize / 2, rcItem.bottom };
                CRenderEngine::DrawLine(hDC, rcLine, pInfo->iVLineSize, GetAdjustColor(pInfo->dwVLineColor));
                rcItem.right -= pInfo->iVLineSize;
            }

            rcItem.left += pInfo->rcTextPadding.left;
            rcItem.right -= pInfo->rcTextPadding.right;
            rcItem.top += pInfo->rcTextPadding.top;
            rcItem.bottom -= pInfo->rcTextPadding.bottom;


            CDuiString item_text;
            if (pCallback != nullptr)
            {
                item_text = pCallback->GetItemText(this, m_iIndex, i);
                SetText(i, item_text);
            }
            else if (GetText(i) != nullptr)
            {
                item_text = GetText(i);
            }
            if (pInfo->bShowHtml)
                CRenderEngine::DrawHtmlText(hDC, m_pManager, rcItem, item_text, iTextColor, &m_rcLinks[m_nLinks], &m_sLinks[m_nLinks], nLinks, pInfo->nFont, pInfo->uTextStyle);
            else
                CRenderEngine::DrawText(hDC, m_pManager, rcItem, item_text, iTextColor, pInfo->nFont, pInfo->uTextStyle);

            m_nLinks += nLinks;
            nLinks = lengthof(m_rcLinks) - m_nLinks;
        }


        for (int i = m_nLinks; i < lengthof(m_rcLinks); i++)
        {
            ::ZeroMemory(m_rcLinks + i, sizeof(RECT));
            ((CDuiString*)(m_sLinks + i))->Empty();
        }
    }

    void CListTextElementUI::InitItemText()
    {
        do 
        {
            if (!m_pOwner)
                break;

            if (m_iIndex < 0)
                break;

            TListInfoUI* pInfo = m_pOwner->GetListInfo();
            if (!pInfo)
                break;

            if (pInfo->nColumns < 0)
                return;

            IListCallbackUI* pCallback = m_pOwner->GetTextCallback();
            if (!pCallback)
                break;

            if (m_aTexts.GetSize() >= pInfo->nColumns)
                break;

            int l = -1;
            CDuiString s;
            while (l < pInfo->nColumns)
            {
                s = pCallback->GetItemText(this, m_iIndex, ++l);
                SetText(l, s);
            }

        } while (0);

    }

        /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListContainerElementUI::CListContainerElementUI() :
        m_iIndex(-1),
        m_iDrawIndex(0),
        m_pOwner(NULL),
        m_bSelected(false),
        m_bExpandable(false),
        m_bExpand(false),
        m_uButtonState(0)
    {}

    LPCTSTR CListContainerElementUI::GetClass() const
    {
        return DUI_CTR_LISTCONTAINERELEMENT;
    }

    UINT CListContainerElementUI::GetControlFlags() const
    {
        return UIFLAG_WANTRETURN;
    }

    LPVOID CListContainerElementUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_ILISTITEM) == 0) return static_cast<IListItemUI*>(this);
        if (_tcscmp(pstrName, DUI_CTR_LISTCONTAINERELEMENT) == 0) return static_cast<CListContainerElementUI*>(this);
        return CContainerUI::GetInterface(pstrName);
    }

    IListOwnerUI* CListContainerElementUI::GetOwner()
    {
        return m_pOwner;
    }

    void CListContainerElementUI::SetOwner(CControlUI* pOwner)
    {
        if (pOwner != NULL) m_pOwner = static_cast<IListOwnerUI*>(pOwner->GetInterface(DUI_CTR_ILISTOWNER));
    }

    void CListContainerElementUI::SetVisible(bool bVisible)
    {
        CContainerUI::SetVisible(bVisible);
        if (!IsVisible() && m_bSelected)
        {
            m_bSelected = false;
            if (m_pOwner != NULL) m_pOwner->SelectItem(-1);
        }
    }

    void CListContainerElementUI::SetEnabled(bool bEnable)
    {
        CControlUI::SetEnabled(bEnable);
        if (!IsEnabled())
        {
            m_uButtonState = 0;
        }
    }

    int CListContainerElementUI::GetIndex() const
    {
        return m_iIndex;
    }

    void CListContainerElementUI::SetIndex(int iIndex)
    {
        m_iIndex = iIndex;
    }

    int CListContainerElementUI::GetDrawIndex() const
    {
        return m_iDrawIndex;
    }

    void CListContainerElementUI::SetDrawIndex(int iIndex)
    {
        m_iDrawIndex = iIndex;
    }

    void CListContainerElementUI::Invalidate()
    {
        if (!IsVisible()) return;

        if (GetParent())
        {
            CContainerUI* pParentContainer = static_cast<CContainerUI*>(GetParent()->GetInterface(DUI_CTR_CONTAINER));
            if (pParentContainer)
            {
                RECT rc = pParentContainer->GetPos();
                RECT rcInset = pParentContainer->GetInset();
                rc.left += rcInset.left;
                rc.top += rcInset.top;
                rc.right -= rcInset.right;
                rc.bottom -= rcInset.bottom;
                CScrollBarUI* pVerticalScrollBar = pParentContainer->GetVerticalScrollBar();
                if (pVerticalScrollBar && pVerticalScrollBar->IsVisible()) rc.right -= pVerticalScrollBar->GetFixedWidth();
                CScrollBarUI* pHorizontalScrollBar = pParentContainer->GetHorizontalScrollBar();
                if (pHorizontalScrollBar && pHorizontalScrollBar->IsVisible()) rc.bottom -= pHorizontalScrollBar->GetFixedHeight();

                RECT invalidateRc = m_rcItem;
                if (!::IntersectRect(&invalidateRc, &m_rcItem, &rc))
                {
                    return;
                }

                CControlUI* pParent = GetParent();
                RECT rcTemp;
                RECT rcParent;
                while (pParent = pParent->GetParent())
                {
                    rcTemp = invalidateRc;
                    rcParent = pParent->GetPos();
                    if (!::IntersectRect(&invalidateRc, &rcTemp, &rcParent))
                    {
                        return;
                    }
                }

                if (m_pManager != NULL) m_pManager->Invalidate(invalidateRc);
            }
            else
            {
                CContainerUI::Invalidate();
            }
        }
        else
        {
            CContainerUI::Invalidate();
        }
    }

    bool CListContainerElementUI::Activate()
    {
        if (!CContainerUI::Activate()) return false;
        if (m_pManager != NULL) m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMACTIVATE);
        return true;
    }

    bool CListContainerElementUI::IsSelected() const
    {
        return m_bSelected;
    }

    bool CListContainerElementUI::Select(bool bSelect, bool bCallback, bool bTriggerEvent)
    {
        bool result = false;
        do
        {
            if (!IsEnabled())
                break;
            result = true;
            if (bSelect == m_bSelected)
                break;
            m_bSelected = bSelect;
            if (bSelect && m_pOwner != NULL)
                m_pOwner->SelectItem(m_iIndex, bTriggerEvent);
            Invalidate();

        } while (0);

        return result;
    }

    bool CListContainerElementUI::IsExpandable() const
    {
        return m_bExpandable;
    }

    void CListContainerElementUI::SetExpandable(bool bExpandable)
    {
        m_bExpandable = bExpandable;
    }

    bool CListContainerElementUI::IsExpanded() const
    {
        return m_bExpand;
    }

    bool CListContainerElementUI::Expand(bool bExpand)
    {
        ASSERT(m_pOwner);
        if (m_pOwner == NULL) return false;
        if (bExpand == m_bExpand) return true;
        m_bExpand = bExpand;
        if (m_bExpandable)
        {
            if (!m_pOwner->ExpandItem(m_iIndex, bExpand)) return false;
            if (m_pManager != NULL)
            {
                if (bExpand) m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMEXPAND, false);
                else m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMCOLLAPSE, false);
            }
        }

        return true;
    }

    void CListContainerElementUI::DoEvent(TEventUI& event)
    {
        if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND)
        {
            if (m_pOwner != NULL) m_pOwner->DoEvent(event);
            else CContainerUI::DoEvent(event);
            return;
        }

        if (event.Type == UIEVENT_DBLCLICK)
        {
            if (IsEnabled())
            {
                Activate();
                Invalidate();
            }
            return;
        }
        if (event.Type == UIEVENT_KEYDOWN)
        {
            if (IsKeyboardEnabled() && IsEnabled())
            {
                if (event.chKey == VK_RETURN)
                {
                    Activate();
                    Invalidate();
                    return;
                }
            }
        }
        if (event.Type == UIEVENT_BUTTONDOWN || event.Type == UIEVENT_RBUTTONDOWN)
        {
            if (IsEnabled())
            {
                m_pManager->SendNotify(this, DUI_MSGTYPE_ITEMCLICK);
                Select();
                Invalidate();
            }
            return;
        }
        if (event.Type == UIEVENT_BUTTONUP)
        {
            return;
        }
        if (event.Type == UIEVENT_MOUSEMOVE)
        {
            return;
        }
        if (event.Type == UIEVENT_MOUSEENTER)
        {
            if (::PtInRect(&m_rcItem, event.ptMouse))
            {
                if (IsEnabled())
                {
                    if ((m_uButtonState & UISTATE_HOT) == 0)
                    {
                        m_uButtonState |= UISTATE_HOT;
                        Invalidate();
                    }
                }
            }
        }
        if (event.Type == UIEVENT_MOUSELEAVE)
        {
            if (!::PtInRect(&m_rcItem, event.ptMouse))
            {
                if (IsEnabled())
                {
                    if ((m_uButtonState & UISTATE_HOT) != 0)
                    {
                        m_uButtonState &= ~UISTATE_HOT;
                        Invalidate();
                    }
                }
                if (m_pManager) m_pManager->RemoveMouseLeaveNeeded(this);
            }
            else
            {
                if (m_pManager) m_pManager->AddMouseLeaveNeeded(this);
                return;
            }
        }

        // An important twist: The list-item will send the event not to its immediate
        // parent but to the "attached" list. A list may actually embed several components
        // in its path to the item, but key-presses etc. needs to go to the actual list.
        if (m_pOwner != NULL) m_pOwner->DoEvent(event); else CControlUI::DoEvent(event);
    }

    void CListContainerElementUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
    {
        if (_tcscmp(pstrName, _T("selected")) == 0) Select();
        else if (_tcscmp(pstrName, _T("expandable")) == 0) SetExpandable(_tcscmp(pstrValue, _T("true")) == 0);
        else CContainerUI::SetAttribute(pstrName, pstrValue);
    }

    bool CListContainerElementUI::DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl)
    {
        DrawItemBk(hDC, m_rcItem);
        return CContainerUI::DoPaint(hDC, rcPaint, pStopControl);
    }

    void CListContainerElementUI::DrawItemText(HDC hDC, const RECT& rcItem)
    {
        return;
    }

    void CListContainerElementUI::DrawItemBk(HDC hDC, const RECT& rcItem)
    {
        ASSERT(m_pOwner);
        if (m_pOwner == NULL) return;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return;
        DWORD iBackColor = 0;
        if (!pInfo->bAlternateBk || m_iDrawIndex % 2 == 0) iBackColor = pInfo->dwBkColor;

        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            iBackColor = pInfo->dwHotBkColor;
        }
        if (IsSelected())
        {
            iBackColor = pInfo->dwSelectedBkColor;
        }
        if (!IsEnabled())
        {
            iBackColor = pInfo->dwDisabledBkColor;
        }
        if (iBackColor != 0)
        {
            CRenderEngine::DrawColor(hDC, m_rcItem, GetAdjustColor(iBackColor));
        }

        if (!IsEnabled())
        {
            if (DrawImage(hDC, pInfo->diDisabled)) return;
        }
        if (IsSelected())
        {
            if (DrawImage(hDC, pInfo->diSelected)) return;
        }
        if ((m_uButtonState & UISTATE_HOT) != 0)
        {
            if (DrawImage(hDC, pInfo->diHot)) return;
        }
        if (!DrawImage(hDC, m_diBk))
        {
            if (!pInfo->bAlternateBk || m_iDrawIndex % 2 == 0)
            {
                if (DrawImage(hDC, pInfo->diBk)) return;
            }
        }
    }

    SIZE CListContainerElementUI::EstimateSize(SIZE szAvailable)
    {
        TListInfoUI* pInfo = NULL;
        SIZE cXY = { 0 };
        do
        {
            if (m_pOwner) pInfo = m_pOwner->GetListInfo();
            if (pInfo == nullptr)
                break;

            cXY = m_cxyFixed;
            if (cXY.cy == 0)
            {
                cXY.cy = pInfo->uFixedHeight;
            }
        } while (0);

        return cXY;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //
    //

    CListHBoxElementUI::CListHBoxElementUI()
    {

    }

    LPCTSTR CListHBoxElementUI::GetClass() const
    {
        return DUI_CTR_LISTHBOXELEMENT;
    }

    LPVOID CListHBoxElementUI::GetInterface(LPCTSTR pstrName)
    {
        if (_tcscmp(pstrName, DUI_CTR_LISTHBOXELEMENT) == 0) return static_cast<CListHBoxElementUI*>(this);
        return CListContainerElementUI::GetInterface(pstrName);
    }

    void CListHBoxElementUI::SetPos(RECT rc, bool bNeedInvalidate)
    {
        if (m_pOwner == NULL) return CListContainerElementUI::SetPos(rc, bNeedInvalidate);

        CControlUI::SetPos(rc, bNeedInvalidate);
        rc = m_rcItem;

        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return;
        if (pInfo->nColumns > 0)
        {
            int iColumnIndex = 0;
            for (int it2 = 0; it2 < m_items.GetSize(); it2++)
            {
                CControlUI* pControl = static_cast<CControlUI*>(m_items[it2]);
                if (!pControl->IsVisible()) continue;
                if (pControl->IsFloat())
                {
                    SetFloatPos(it2);
                    continue;
                }
                if (iColumnIndex >= pInfo->nColumns) continue;

                RECT rcPadding = pControl->GetPadding();
                RECT rcItem = { pInfo->rcColumn[iColumnIndex].left + rcPadding.left, m_rcItem.top + rcPadding.top,
                    pInfo->rcColumn[iColumnIndex].right - rcPadding.right, m_rcItem.bottom - rcPadding.bottom };
                if (pInfo->iVLineSize > 0 && iColumnIndex < pInfo->nColumns - 1)
                {
                    rcItem.right -= pInfo->iVLineSize;
                }
                pControl->SetPos(rcItem, false);
                iColumnIndex += 1;
            }
        }
        else
        {
            for (int it2 = 0; it2 < m_items.GetSize(); it2++)
            {
                CControlUI* pControl = static_cast<CControlUI*>(m_items[it2]);
                if (!pControl->IsVisible()) continue;
                if (pControl->IsFloat())
                {
                    SetFloatPos(it2);
                    continue;
                }

                RECT rcPadding = pControl->GetPadding();
                RECT rcItem = { m_rcItem.left + rcPadding.left, m_rcItem.top + rcPadding.top,
                    m_rcItem.right - rcPadding.right, m_rcItem.bottom - rcPadding.bottom };
                pControl->SetPos(rcItem, false);
            }
        }
    }

    bool CListHBoxElementUI::DoPaint(HDC hDC, const RECT& rcPaint, CControlUI* pStopControl)
    {
        ASSERT(m_pOwner);
        if (m_pOwner == NULL) return true;
        TListInfoUI* pInfo = m_pOwner->GetListInfo();
        if (pInfo == NULL) return true;

        DrawItemBk(hDC, m_rcItem);
        for (int i = 0; i < pInfo->nColumns; i++)
        {
            RECT rcItem = { pInfo->rcColumn[i].left, m_rcItem.top, pInfo->rcColumn[i].right, m_rcItem.bottom };
            if (pInfo->iVLineSize > 0 && i < pInfo->nColumns - 1)
            {
                RECT rcLine = { rcItem.right - pInfo->iVLineSize / 2, rcItem.top, rcItem.right - pInfo->iVLineSize / 2, rcItem.bottom };
                CRenderEngine::DrawLine(hDC, rcLine, pInfo->iVLineSize, GetAdjustColor(pInfo->dwVLineColor));
            }
        }
        return CContainerUI::DoPaint(hDC, rcPaint, pStopControl);
    }

} // namespace DuiLib

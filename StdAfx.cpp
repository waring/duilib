// stdafx.cpp : source file that includes just the standard includes
//	UIlib.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "StdAfx.h"


#pragma comment( lib, "winmm.lib" )
#pragma comment( lib, "comctl32.lib" )

void debughelper::DebugPrint(__in_range(0, INT16_MAX) size_t _length,
    _In_z_ LPCTSTR lpszFormat, ...)
{
#ifdef  OUT_DEBUG_STRING  // 打印调试信息
    do
    {
        if (lpszFormat == nullptr)
            break;

        _length = _length == 0 ? INT16_MAX : _length;
        _length = _length > INT16_MAX ? INT16_MAX : _length + 1;

        va_list args;
        errno_t nBuf;
        TCHAR* szBuf = nullptr;
        try
        {
            szBuf = new TCHAR[_length];
            memset(szBuf, 0, _length * sizeof(TCHAR));
        }
        catch (const std::bad_alloc&)
        {
            // error_msg.what()
            OutputDebugString(L"无法分配内存\n");
            szBuf = nullptr;
            break;
        }

        if (nullptr == szBuf)
            break;

        va_start(args, lpszFormat);
        nBuf = _vsntprintf_s(szBuf, _length, _TRUNCATE, lpszFormat, args);
        OutputDebugString(szBuf);
        va_end(args);

        delete[] szBuf;
        szBuf = nullptr;
    } while (0);
#endif //  OUT_DEBUG_STRING
}
extern WyAppUiLanguage the_app_ui_language = WyAppUiLanguage::kChines;
